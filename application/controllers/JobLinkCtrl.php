<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class JobLinkCtrl extends CI_Controller
{

    public function index()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        $this->load->model("JobLink_m");

        $job_link = $this->JobLink_m->all_job_link();

        $data['job_link'] = $job_link;

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('job_links',$data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function save_link()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');


        if (isset($_POST['link'])) {
            $this->load->model("JobLink_m");
            $this->JobLink_m->save_link($this->input->post("link"));
            
            $this->session->set_flashdata(array("title"=>"Success","text"=>"Link Saved !!"));
            redirect(base_url("/JobLinkCtrl"));
        }
    }

    public function del_link()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');


        if (isset($_GET['id'])) {
            $this->load->model("JobLink_m");
            $this->JobLink_m->delete_link($this->input->get("id"));
            
            $this->session->set_flashdata(array("title"=>"Success","text"=>"Link Deleted !! "));
            redirect(base_url("/JobLinkCtrl"));
        }
    }
}
