<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class DashboardCtrl extends CI_Controller

{

    public function index()

    {

        if (isset($_SESSION['sec_rf_admin'])) {

            $_SESSION['sec_rf_admin'] == true ? "" : redirect('/');

        }else{

            redirect('/');

        }

        $this->load->model('Dashboard_m');
        $data['actv_mem']=$this->Dashboard_m->count_active_member();
        $data['inactv_mem']=$this->Dashboard_m->count_inactive_member();
        $data['pend_mem']=$this->Dashboard_m->count_pend_member();
        $data['today_mem_add']=$this->Dashboard_m->count_member_add(0);
        $data['svn_mem_add']=$this->Dashboard_m->count_member_add(7);
        $data['thirty_mem_add']=$this->Dashboard_m->count_member_add(30);
        $data['today_withdraw']=$this->Dashboard_m->total_withdraw(0);
        $data['svn_withdraw']=$this->Dashboard_m->total_withdraw(7);
        $data['thirty_withdraw']=$this->Dashboard_m->total_withdraw(30);
        $data['total_withdraw']=$this->Dashboard_m->total_withdraw(99999);
        $data['top_referers']=$this->Dashboard_m->top_5_refer();
        $data['users_earned']=$this->Dashboard_m->user_earned();
        $data['reg_income_tdy']=$this->Dashboard_m->total_reg_fee_added(0);
        $data['reg_income_svn']=$this->Dashboard_m->total_reg_fee_added(7);
        $data['reg_income_thirty']=$this->Dashboard_m->total_reg_fee_added(30);
        $data['reg_income_total']=$this->Dashboard_m->total_reg_fee_added(99999);
        


        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');



        $this->load->view('dashboard',$data); //4_dynamic



        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');



    }

}