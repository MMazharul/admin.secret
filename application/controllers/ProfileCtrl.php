<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class ProfileCtrl extends CI_Controller

{

    public function __construct()
    {
            parent::__construct();
            $this->load->helper(array('form', 'url'));
    }

    public function index()

    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');
        
        if (isset($_GET['user_id'])) {

            $this->load->model('Member_m');

            $user_data = $this->Member_m->get_user_all_data($this->input->get("user_id"));
            $refered_mem = $this->Member_m->get_refered_mem($this->input->get("user_id"));

            $data['user_data'] = $user_data[0];
            $data['refered_mem'] = $refered_mem;

            $this->load->view('templates/1_head.php');

            $this->load->view('templates/2_nav.php');

            $this->load->view('templates/3_sidebar_menu_left.php');

            $this->load->view('profile', $data); //4_dynamic

            $this->load->view('templates/5_footer.php');

            $this->load->view('templates/6_script_end.php');
        } else {
            redirect("/");
        }


    }

    public function resend_credential()
    {
        if(isset($_POST['id'])){
            $this->load->model('Member_m');
            $user = $this->Member_m->get_user_mobile($_POST['id']);

            $this->load->library('Sms_send');
            $this->sms_send->send_sms($user->mobile_num,"Hello ".$user->name.". Your Biz-Bazar Mobile Number is: 0".$user->mobile_num." and Activation Code: ".$user->activation_pin."  login here : https://biz-bazar.com");
            echo json_encode(array("success"=>1));
        }
    }

    
    public function profile_edit()
    {

        if (isset($_POST['name'])) {
                $this->load->model("Member_m");
                $user_id = $this->input->post('user_id');
                
                $name = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($_POST['name']))))));
                $mob = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($_POST['mob']))))));
                $gender = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($_POST['gender']))))));
                $email = trim(strip_tags($_POST['email']));
                $adress = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($_POST['adress']))))));
                $nid = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 ]/', ' ', urldecode(html_entity_decode(strip_tags($_POST['nid']))))));

                if(!empty($_FILES)){
                    $picture = $this->session->user_id.".png";
                    $config['upload_path']= '../../../work.biz-bazar/app-assets/images/members/';
                    $config['allowed_types']    = 'gif|jpg|png';
                    $config['file_name']        =   $picture;
                    $config['overwrite']        =   true;
                    $this->load->library('upload',$config);
                    $image_data = $this->upload->data();
                   // @unlink('./../../../app-assets/images/members/'.$picture);

                    if($this->upload->do_upload('photo')){
                        $this->Member_m->update_profile_with_pic($name,$gender,$email,$adress,$nid,$picture,$user_id);
                        print_r( $this->upload->data());
                    }
                    print_r($this->upload->display_errors());
                    
                    
                }

                $this->Member_m->update_profile_no_pic($name,$gender,$email,$adress,$nid,$user_id);
            
        }
    }

    public function change_pass()
    {
        if(isset($_POST['c_pass'])){
            $pass = $this->input->post("pass");
            $c_pass = $this->input->post("c_pass");
            $user_id = $this->input->post("id");
            
            if($pass === $c_pass){
                $this->load->model('Member_m');
                    $new_pass = password_hash($pass,PASSWORD_DEFAULT);
                    $this->Member_m->update_pass($user_id,$new_pass);
                    echo(json_encode(array("response"=>'<span class="p-1 border10 bg-success">Successfully,Updated Password!</span>',"success"=>"1")));

                    if($this->input->post('user_mob') == 1){
                        $user = $this->Member_m->get_user_mobile($user_id);
                        $this->load->library('Sms_send');
                        $this->sms_send->send_sms($user->mobile_num,'Dear Biz-Bazar User, Your Password has changed for some security reason. Your New pass word is: '.$pass.' Thank You.');
                    }
            }
            else{
                echo(json_encode(array("response"=>'<span class="p-2 border10 bg-danger">Password Not Matched</span>')));
            }
        }
    }

    
}