<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class NoticeCtrl extends CI_Controller{

    public function index()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        $this->load->model('Notice_m');
        $data['notices'] = $n = $this->Notice_m->all_notice();

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('notice',$data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function save_notice()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        if(isset($_POST['content'])){
            $this->load->model('Notice_m');
            $this->Notice_m->save_notice(preg_replace('/(\'*)/', '', $this->input->post("content")));

            $this->session->set_flashdata(array("title"=>"Success","text"=>"Notice Saved !!"));
            redirect('/NoticeCtrl');
        }
    }

    public function del_notice()
    {
       if(isset($_GET['id'])){
        $this->load->model('Notice_m');
        $this->Notice_m->del_notice($this->input->get("id"));

        $this->session->set_flashdata(array("title"=>"Success","text"=>"Notice Deleted !!"));
        redirect('/NoticeCtrl');
       } 
    }
}