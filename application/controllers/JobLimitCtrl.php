<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class JobLimitCtrl extends CI_Controller
{

    public function index()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        $this->load->model('JobLimit_m');

        $data['limited_acc'] = $this->JobLimit_m->get_limited_acc();
        $data['non_limited_acc'] = $this->JobLimit_m->get_non_limited_acc();

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('job_limit', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function set_limit()
    {
        if (isset($_POST['u_id'])) {
            $this->load->model('JobLimit_m');
            $this->JobLimit_m->set_user_limit($this->input->post('u_id'), $this->input->post('limit'));
            echo json_encode(array('success' => 1));
        }
    }

    public function edit_limit()
    {
        if (isset($_POST['u_id'])) {
            $this->load->model('JobLimit_m');
            $this->JobLimit_m->edit_user_limit($this->input->post('u_id'), $this->input->post('limit'));
            echo json_encode(array('success' => 1));
        }
    }

    public function delete_limit()
    {
        if (isset($_GET['user_id'])) {
            $this->load->model('JobLimit_m');
            $this->JobLimit_m->delete_limit($this->input->get('user_id'));
            redirect('JobLimitCtrl');
        }
    }

    public function update_last_refer_date_toall()
    {
        $q = $this->db->query("SELECT id,own_refer_id FROM `users_detail`");
        $rs = $q->result();

        foreach ($rs as $mem) {
            $this->db->query("UPDATE `users_login` SET `last_refer_date`= (SELECT IFNULL((SELECT acc_active_date FROM `users_detail` WHERE refer_from = '$mem->own_refer_id' and  acc_active_date is not null ORDER BY acc_active_date DESC LIMIT 1),(SELECT acc_active_date FROM users_detail WHERE own_refer_id = '$mem->own_refer_id')) last_refer) WHERE user_id='$mem->id'");
            echo "<br>";
            echo $mem->id . "--" . $mem->own_refer_id;
            echo "<br>";
        }
    }



    public function mass_remove_limit()
    {
        $this->load->model('JobLimit_m');
        $r =  $this->JobLimit_m->mass_remove_limit($this->input->get('min_day'));

        foreach ($r as $user) {
            echo $user->user_id . " Deleted <br>";
            $user_id = $user->user_id;
            $this->db->query("Delete from job_limit where user_id='$user_id'");
        }
    }

    public function mass_add_limit()
    {
        $this->load->model('JobLimit_m');
        $r =  $this->JobLimit_m->mass_add_limit($this->input->get('min_ref'));

        foreach ($r as $user) {
            echo $user->user_id . " Deleted <br>";
            $user_id = $user->user_id;
            $this->db->query("INSERT INTO `job_limit`(`user_id`, `job_limit`) VALUES ('$user_id',5)");
        }
    }
}
