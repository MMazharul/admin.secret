<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LoginCtrl extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('login');
	}

	public function login_req(){
		
		if(isset($_POST['admin_num'])){
			$admin_num = trim($this->input->post('admin_num'));
			$pass = sha1(md5(md5(trim($this->input->post('pass')))));

			
			if($admin_num !=null && $pass !=null){
				$this->db->select('*');
				$this->db->from('admin');
				$this->db->where(array('admin_number'=>$admin_num,"password"=>$pass));
				$query = $this->db->get();
				$rs = $query->result();
				

				if($rs !=null ){

					$array = array(
						'sec_rf_admin' => true
					);
					
					$this->session->set_userdata( $array );
					
					redirect('DashboardCtrl');
				}
				else{
					redirect("/");
				}
				
			}
			else{
				redirect("/");
			}

		}
		else{
			redirect("/");
		}
	}

	public function logout()
	{
		session_unset();
		session_destroy();
		redirect('/');
	}
}
