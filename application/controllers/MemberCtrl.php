<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');



class MemberCtrl extends CI_Controller

{



    public function index()
    {

        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        redirect('/MemberCtrl/member_rqst');

    }



    public function member_rqst()

    {

        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');


        $data['member_reqsts'] = $this->Member_m->get_req_mem_for_activation();
        $data['member_reqsts_his'] = $this->Member_m->get_activation_his();


        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');



        $this->load->view('member_rqst', $data); //4_dynamic



        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');

    }



    public function member_activation()

    {
          isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        $user_id = $this->input->post('user_id');
        $id = $this->input->post('id');

        $referer_refer_id = $this->input->post('referer_refer_id');
        $mob = $this->input->post('mob');

        if ($_POST['activate']) {

            $this->load->library('Sms_send');
            $this->db->db_debug = true;

            $this->Member_m->set_user_lvl($user_id);
            $this->Member_m->setUserActive_and_updateActiveRqst($user_id, $id);
            if ($this->Member_m->set_balance($user_id)) {

                $this->Member_m->set_refer_user_instant_refer_earn($referer_refer_id, $user_id);

                $this->Member_m->update_referer_refer_earn_balance($referer_refer_id);

                $this->Member_m->update_referer_lvl($referer_refer_id);

                $this->Member_m->update_referer_last_refer_date($referer_refer_id);
            }


            $total_refered = $this->Member_m->get_referer_total_refered($referer_refer_id);
            $pro_levels = $this->Member_m->get_pro_levels();

            if ($this->Member_m->set_refer_user_always_earn($user_id, $referer_refer_id)) {

                if ($total_refered[0]->total_refer == $pro_levels[0]->total_refer_member) {
                    $this->Member_m->set_referer_to_pro_level($referer_refer_id, $pro_levels[0]->percentage);
                } elseif ($total_refered[0]->total_refer == $pro_levels[1]->total_refer_member) {
                    $this->Member_m->set_referer_to_pro_level($referer_refer_id, $pro_levels[1]->percentage);
                } elseif ($total_refered[0]->total_refer == $pro_levels[2]->total_refer_member) {
                    $this->Member_m->set_referer_to_pro_level($referer_refer_id, $pro_levels[2]->percentage);
                } elseif ($total_refered[0]->total_refer == $pro_levels[3]->total_refer_member) {
                    $this->Member_m->set_referer_to_pro_level($referer_refer_id, $pro_levels[3]->percentage);
                } elseif ($total_refered[0]->total_refer == $pro_levels[4]->total_refer_member) {
                    $this->Member_m->set_referer_to_pro_level($referer_refer_id, $pro_levels[4]->percentage);
                }

                    // 2nd level referer earn percentage
                $this->Member_m->second_lvl_referer_percentage($referer_refer_id);

                    // 3rd level referer earn percentage
                $this->Member_m->third_lvl_referer_percentage($referer_refer_id);
            
                    // 4th level referer earn percentage
                $this->Member_m->forth_lvl_referer_percentage($referer_refer_id);

                    // 5th level referer earn percentage
                $fifth_ref_id = $this->Member_m->get_fifth_lvl_referer_id($referer_refer_id);
                $this->Member_m->fifth_lvl_referer_percentage($fifth_ref_id);
            }


            $this->sms_send->send_sms($mob, "Congratulation! Your Account Has Been Activated Successfully! Now You Can Earn Freelance From Biz-Bazar.");

            $this->session->set_flashdata(array("title" => "Success", "text" => "New Member Added !!"));
           
            echo json_encode(array('success'=>1));


        }

    }



    public function member_decline()

    {

        if (isset($_POST['user_id'])) {
            $this->db->db_debug = false;
            $this->Member_m->delete_reqst_and_reset_status($_POST['user_id'], $_POST['id']);
            $this->session->set_flashdata(array("title" => "Decline", "text" => "Member Request Declined !!"));
            echo json_encode(array('success'=>1));

        }

    }

    public function all_members()
    {
        if (isset($_GET['notify_id'])) {
            $this->load->model('Notify_m');
            $this->Notify_m->seen_notify($_GET['notify_id']);
        }

        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');


        $data['members'] = $this->Member_m->get_all_members();
        $data['i_members'] = $this->Member_m->get_inactive_members();
        $data['p_members'] = $this->Member_m->get_pending_members();
        $data['b_members'] = $this->Member_m->get_blocked_members();


        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('members', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function refer_network()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');


        $data['networks'] = $this->Member_m->get_refer_network();


        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('refer_network', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function matured_acc()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        $data['matureds'] = $this->Member_m->get_withdrawable_mem();

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('matured_acc', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function matured_acc_job()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        $data['matureds'] = $this->Member_m->get_withdrawable_mem_job();

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('matured_acc_job', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function matured_acc_refer()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        $data['matureds'] = $this->Member_m->get_withdrawable_mem_refer();

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('matured_acc_refer', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function remove_member()
    {
        if (isset($_GET['user_id'])) {
            $this->Member_m->remove_member($this->input->get('user_id'));
            $this->session->set_flashdata(array("title" => "Success", "text" => "Member Removed !!"));
            redirect("MemberCtrl/all_members");
        }
    }

    public function block_user()
    {
        if (isset($_POST['id'])) {
            $this->Member_m->block($this->input->post('id'));
            echo json_encode(array('success' => 1));
        }
    }

    public function unblock_user()
    {
        if (isset($_POST['id'])) {
            $this->Member_m->unblock($this->input->post('id'));
            echo json_encode(array('success' => 1));
        }
    }

    public function send_sms()
    {
        if (isset($_POST['mob'])) {
            $this->load->library('Sms_send');
            $this->sms_send->send_sms($this->input->post('mob'), $this->input->post('txt'));
            echo json_encode(array('success' => 1));
        }
    }

    public function send_notify()
    {
        if (isset($_POST['user_id'])) {
            $this->load->model('Notify_m');
            $this->Notify_m->set_notify_user('New Notification From Biz-Bazar', $this->input->post('txt'), 'NotifyCtrl', $this->input->post('user_id'), '<div class="preview-icon bg-warning"><i class="mdi mdi-volume-low"></i></div>');
            echo json_encode(array('success' => 1));
        }
    }

    public function send_bonus()
    {
        if (isset($_POST['user_id'])) {
            $this->load->model('Notify_m');
            $this->load->library('Sms_send');

            $this->Member_m->set_bonus($this->input->post('user_id'), $this->input->post('amnt'));

            $this->sms_send->send_sms($this->input->post('mob'), $this->input->post('s_txt'));

            $this->Notify_m->set_notify_user('New Balance From Biz-Bazar', $this->input->post('n_txt'), 'NotifyCtrl', $this->input->post('user_id'), '<div class="preview-icon bg-success"><i class="mdi mdi-square-inc-cash"></i></div>');

            echo json_encode(array('success' => 1));
        }
    }
}