<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller{



	public function __construct() {
		parent::__construct();

		$this->load->model('common_m');
	}



	public function package_edit()
	{
		$data['edit_data']=[];
		$id=$this->input->get('id');

		if(!empty($id))
		{
			$row=$this->common_m->getRow('flexiload_package','*','id',$id);

			if($row!==false)
			{
				$data['package']=$row;
			}
			else
			{
				echo "eror";
				die;
			}

		}
		$this->load->view('templates/1_head.php');

		$this->load->view('templates/2_nav.php');

		$this->load->view('templates/3_sidebar_menu_left.php');

		$this->load->view('settings/flexiload_package/edit',$data); //4_dynamic

		$this->load->view('templates/5_footer.php');

		$this->load->view('templates/6_script_end.php');
	}

	public function package_store($id='')
	{
		if(!empty($id))
		{
			$update_data=$this->input->post();

			$this->common_m->updateData('flexiload_package','id',$id, $update_data);

			$this->session->set_flashdata(array("title" => "Success", "text" => "Flexiload Package Successfully Updated!!"));

			redirect('setting/package_list');
		}
		else
		{
			$store_data=$this->input->post();
			$this->common_m->insert('flexiload_package',$store_data);
			$this->session->set_flashdata(array("title" => "Success", "text" => "Flexiload Package Successfully added!!"));
			redirect('setting/package_list');

		}
	}

	public function package_list()
	{
		$data['packages']=$this->common_m->select_with_where('flexiload_package','*','status!=2');

		$this->load->view('templates/1_head.php');

		$this->load->view('templates/2_nav.php');

		$this->load->view('templates/3_sidebar_menu_left.php');

		$this->load->view('settings/flexiload_package/list',$data); //4_dynamic

		$this->load->view('templates/5_footer.php');

		$this->load->view('templates/6_script_end.php');

	}

	public function update_status()
	{
		$id=$this->input->post('id');
		$data['status']=($this->input->post('status')==1) ? 0 : (1);

		$result=$this->common_m->updateData('flexiload_package','id',$id, $data);
		$this->session->set_flashdata(array("title" => "Success", "text" => "Status Change Successfully !!"));
		if($result!==false)
		{
			echo json_encode(array('success'=>true));
		}
		else{
			echo json_encode(array('success'=>false));

		}

	}
	public function package_delete()
	{
		$id=$this->input->get('id');
		$result=$this->common_m->delete('flexiload_package','id',$id);
		if($result!==false)
		{
			$this->session->set_flashdata(array("title" => "Success", "text" => "Flexiload Package Delete Successfully !!"));
			redirect('setting/package_list');
		}

	}

	public function flexiload_setting($id='')
	{
		if(!empty($id))
		{
			$update_data = $this->input->post();

			$this->common_m->updateData('flexiload_payment_info','id',$id,$update_data);

			$this->session->set_flashdata(array("title" => "Success", "text" => "Updated Successfully done!!"));
			 redirect('Setting/flexiload_setting');
		}

		$data['setting']=$this->common_m->select_all('flexiload_payment_info','*');
		$data['setting']=$data['setting'][0];

		$this->load->view('templates/1_head.php');

		$this->load->view('templates/2_nav.php');

		$this->load->view('templates/3_sidebar_menu_left.php');

		$this->load->view('settings/flexiload_setting',$data); //4_dynamic

		$this->load->view('templates/5_footer.php');

		$this->load->view('templates/6_script_end.php');

	}



}
