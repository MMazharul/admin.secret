<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

class ReportCtrl extends CI_Controller
{

    public function index()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        $this->load->model('Report_m');

        if (isset($_GET['from']) && isset($_GET['to'])) {
          
            $from =date("Y-m-d H:i:s", strtotime($this->input->get('from')));
            $to =date("Y-m-d H:i:s", strtotime($this->input->get('to')));

            $data['all_total'] = $this->Report_m->search_total_earned($from,$to);
            $data['income'] = $this->Report_m->search_total_income($from,$to);
            $data['all_members_total'] = $this->Report_m->search_all_members_total_earned($from,$to);

        } else {

            $data['all_total'] = $this->Report_m->get_total_earned();
            $data['income'] = $this->Report_m->get_total_income();
            $data['all_members_total'] = $this->Report_m->get_all_members_total_earned();

        }



        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('report', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function search_report()
    {
        if (isset($_GET['from']) && isset($_GET['to'])) {
            echo $this->input->get('from') . "<br>";
            echo $this->input->get('to') . "<br>";
            echo date("Y-m-d H:i:s", strtotime($this->input->get('from'))) . "<br>";
            echo date("Y-m-d H:i:s", strtotime($this->input->get('to'))) . "<br>";
        } else {

        }
    }

    public function report_details()
    {
        if (isset($_GET['from']) && isset($_GET['to'])) {

            $from =date("Y-m-d H:i:s", strtotime($this->input->get('from')));
            $to =date("Y-m-d H:i:s", strtotime($this->input->get('to')));

            if (isset($_GET['refer_earned_id'])) {
                $this->load->model('Report_m');
                $data['datas'] = $this->Report_m->search_all_refer_bonus_earned($this->input->get('refer_earned_id'),$from,$to);
                $intrvl = date_diff(date_create($from),date_create($to));
                $diff = $intrvl->format("%y Year %m Month %d Day %h Hours %i Minute %s Seconds");
                $data['title'] = 'All Refer Bonus Earned ( From: <u>'.$_GET['from'].'</u> To: <u>'.$_GET['to'].'</u> )';
                $data['date_diff'] = $diff;
                $data['from_user'] = 1;
            } else if (isset($_GET['refer_work_earned_id'])) {
                $this->load->model('Report_m');
                $data['datas'] = $this->Report_m->search_all_refer_work_earned($this->input->get('refer_work_earned_id'),$from,$to);
                $intrvl = date_diff(date_create($from),date_create($to));
                $diff = $intrvl->format("%y Year %m Month %d Day %h Hours %i Minute %s Seconds");
                $data['title'] = 'All Refer Work Earned  ( From: <u>'.$_GET['from'].'</u> To: <u>'.$_GET['to'].'</u> )';
                $data['date_diff'] = $diff;
                $data['from_user'] = 1;
            } else if (isset($_GET['work_earned_id'])) {
                $this->load->model('Report_m');
                $data['datas'] = $this->Report_m->search_all_work_earned($this->input->get('work_earned_id'),$from,$to);
                $intrvl = date_diff(date_create($from),date_create($to));
                $diff = $intrvl->format("%y Year %m Month %d Day %h Hours %i Minute %s Seconds");
                $data['date_diff'] = $diff;
                $data['title'] = 'All Work Earned ( From: <u>'.$_GET['from'].'</u> To: <u>'.$_GET['to'].'</u> )';
            } else if (isset($_GET['other_earned_id'])) {
                $this->load->model('Report_m');
                $data['datas'] = $this->Report_m->search_all_other_earned($this->input->get('other_earned_id'),$from,$to);
                $intrvl = date_diff(date_create($from),date_create($to));
                $diff = $intrvl->format("%y Year %m Month %d Day %h Hours %i Minute %s Seconds");
                $data['date_diff'] = $diff;
                $data['title'] = 'All Other Earned  ( From: <u>'.$_GET['from'].'</u> To: <u>'.$_GET['to'].'</u> )';
            } else if (isset($_GET['withdrawn_id'])) {
                $this->load->model('Report_m');
                $data['datas'] = $this->Report_m->search_all_withdraw($this->input->get('withdrawn_id'),$from,$to);
                $intrvl = date_diff(date_create($from),date_create($to));
                $diff = $intrvl->format("%y Year %m Month %d Day %h Hours %i Minute %s Seconds");
                $data['date_diff'] = $diff;
                $data['title'] = 'All Withdrawn  ( From: <u>'.$_GET['from'].'</u> To: <u>'.$_GET['to'].'</u> )';
            }


        }
        else{

            if (isset($_GET['refer_earned_id'])) {
                $this->load->model('Report_m');
                $data['datas'] = $this->Report_m->get_all_refer_bonus_earned($this->input->get('refer_earned_id'));
                $data['title'] = 'All Refer Bonus Earned';
                $data['from_user'] = 1;
            } else if (isset($_GET['refer_work_earned_id'])) {
                $this->load->model('Report_m');
                $data['datas'] = $this->Report_m->get_all_refer_work_earned($this->input->get('refer_work_earned_id'));
                $data['title'] = 'All Refer Work Earned';
                $data['from_user'] = 1;
            } else if (isset($_GET['work_earned_id'])) {
                $this->load->model('Report_m');
                $data['datas'] = $this->Report_m->get_all_work_earned($this->input->get('work_earned_id'));
                $data['title'] = 'All Work Earned';
            } else if (isset($_GET['other_earned_id'])) {
                $this->load->model('Report_m');
                $data['datas'] = $this->Report_m->get_all_other_earned($this->input->get('other_earned_id'));
                $data['title'] = 'All Other Earned';
            } else if (isset($_GET['withdrawn_id'])) {
                $this->load->model('Report_m');
                $data['datas'] = $this->Report_m->get_all_withdraw($this->input->get('withdrawn_id'));
                $data['title'] = 'All Withdrawn';
            }

        }
        


        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('report_details', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');

    }
}
