<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BusinessLvlCtrl extends CI_Controller{

    public function index()
    {

        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        $this->load->model('Business_level_m');
        $data['levels'] = $this->Business_level_m->get_all_levels();


        $data['join_fee'] = $this->Business_level_m->get_join_fee();



        $data['earn_pl'] = $this->Business_level_m->get_earn_perlink();
        $data['w_alw'] = $this->Business_level_m->get_withdraw_allow_status();



        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('business_level',$data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function update_level(){
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        if(isset($_POST['id'])){
            $this->load->model('Business_level_m');
            $this->Business_level_m->update_level($this->input->post('id'),$this->input->post('refer'),$this->input->post('percent'));
            echo json_encode(array("success"=>1));
        }
    }

    public function update_join_fee(){
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        if(isset($_POST['id'])){
            $this->load->model('Business_level_m');
            $this->Business_level_m->update_join_fee($this->input->post('id'),$this->input->post("snd_num"),$this->input->post('fee'),$this->input->post('type'),$this->input->post('rf_percent'),$this->input->post('scnd_rf_percent'),$this->input->post('trd_rf_percent'),$this->input->post('frth_rf_percent'),$this->input->post('fifth_rf_percent'));
            echo json_encode(array("success"=>1));
        }
    }

    public function update_earn_per_link()
    {
        if(isset($_POST['id'])){
            $this->load->model('Business_level_m'); 
            $this->Business_level_m->update_earn_per_link($this->input->post('val'));
            echo json_encode(array("success"=>1));
        }
    }

    public function change_withdraw_status()
    {
        if(isset($_POST['allow'])){
            $this->load->model('Business_level_m'); 
            $this->Business_level_m->update_withdraw_allow($this->input->post('allow'));
            echo json_encode(array('success'=>1));
        }
    }
}
