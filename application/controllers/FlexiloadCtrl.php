<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FlexiloadCtrl extends CI_Controller
{
	public function index()
	{

	}

	public function load_fund_req()
	{
		$this->load->model('common_m');

		isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

		$data['fund_reqsts']=$this->common_m->single_join_query(
			'fq.*,fp.package_amount,fp.total_commission', 'flexiload_fund_req as fq',
			'flexiload_package as fp', 'fq.package_id=fp.id',
			'fq.status=0',
			'created_at','DESC'
		);

		$data['fund_reqsts_his']=$this->common_m->single_join_query(
			'fq.*,fp.package_amount', 'flexiload_fund_req as fq',
			'flexiload_package as fp', 'fq.package_id=fp.id',
			'fq.status=1 OR fq.status=2',
			'created_at','DESC'
		);

		$this->load->view('templates/1_head.php');

		$this->load->view('templates/2_nav.php');

		$this->load->view('templates/3_sidebar_menu_left.php');

		$this->load->view('flexiload/fund_request', $data); //4_dynamic

		$this->load->view('templates/5_footer.php');

		$this->load->view('templates/6_script_end.php');
	}

	public function fund_accepted()
	{
		$this->load->model('common_m');

		$msg="Congratulations your fund added successfully done.Thanks for Biz-Bazar.com";

		$user_id=$this->input->post('user_id');

		$package_amount=$this->input->post('package_amount');

		$id=$this->input->post('id');

		$exist_balance=$this->common_m->getRow('flexiload_balance','blance','user_id',$user_id);

		$data['success']=false;

		$this->db->trans_start();

		if($exist_balance!==false)
		{
			$new_blance=$exist_balance->blance+$package_amount;

			$this->common_m->updateData('flexiload_balance','user_id',$user_id,array('blance'=>$new_blance));
		}
		else{
			$this->common_m->insert('flexiload_balance',array('user_id'=>$user_id,'blance'=>$package_amount));
		}

		$this->common_m->updateData('flexiload_fund_req','id',$id,array('status'=>1));

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();

			$data['success']=true;
			echo json_encode($data);

		}

	}

	public function fund_rejected()
	{
		$this->load->model('common_m');

		$id=$this->input->post('id');

		$result = $this->common_m->updateData('flexiload_fund_req','id',$id,array('status'=>2));

		if($result!==false)
			echo json_encode(array('success'=>true));
		else
			echo json_encode(array('success'=>false));

	}

}
