<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AlertCtrl extends CI_Controller{

    public function index()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');


        $q = $this->db->query("SELECT * FROM `sent_sms` ORDER BY created_at DESC");
        $qr = $this->db->query("SELECT * FROM `notifications` ORDER BY created_at DESC");

        $data['all_sms'] = $q->result();
        $data['all_notification'] = $qr->result();

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('alert',$data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function send_sms_to_members()
    {
        if(isset($_POST['sms_txt']) && isset($_POST['type'])){

            if($this->input->post('type') == "sms"){

                $this->load->library('Sms_send');
            
                if($_POST['target']=='all'){
                    $all_mem = $this->Member_m->get_all_members();
                    foreach($all_mem as $key => $mem){
                        $this->sms_send->send_sms($mem->mobile_num,$this->input->post('sms_txt'));
                    }
                    $this->db->insert('sent_sms', array("text"=>preg_replace('/(\'*)/', '',$this->input->post('sms_txt')),"target"=>"All Members"));
                    echo json_encode(array("process"=>"complete"));
                }
    
                else if($_POST['target']=='active'){
                    $actv_mem = $this->Member_m->get_active_members();
                    foreach($actv_mem as $key => $mem){
                        $this->sms_send->send_sms($mem->mobile_num,$this->input->post('sms_txt'));
                    }
                    $this->db->insert('sent_sms', array("text"=>preg_replace('/(\'*)/', '',$this->input->post('sms_txt')),"target"=>"Active Members"));
                    echo json_encode(array("process"=>"complete"));
                }
    
                else if($_POST['target']=='inactive'){
                    $inactv_mem = $this->Member_m->get_inactive_members();
                    foreach($inactv_mem as $key => $mem){
                        $this->sms_send->send_sms($mem->mobile_num,$this->input->post('sms_txt'));
                    }
                    $this->db->insert('sent_sms', array("text"=>preg_replace('/(\'*)/', '',$this->input->post('sms_txt')),"target"=>"Inactive Members"));
                    echo json_encode(array("process"=>"complete"));
                }
    
                else if($_POST['target']=='pending'){
                    $pend_mem = $this->Member_m->get_pending_members();
                    foreach($pend_mem as $key => $mem){
                        $this->sms_send->send_sms($mem->mobile_num,$this->input->post('sms_txt'));
                    }
                    $this->db->insert('sent_sms', array("text"=>preg_replace('/(\'*)/', '',$this->input->post('sms_txt')),"target"=>"Pending Members"));
                    echo json_encode(array("process"=>"complete"));
                }

            }
            else if($this->input->post('type') == "notification"){

               /*  database
                target 0 for inactive
                target 1 for active
                target 2 for pending
                target 3 for all 
                target 4 for admin */
            
                if($_POST['target']=='all'){
                    $this->db->insert('notifications', array("notification_txt"=>preg_replace('/(\'*)/', '',$this->input->post('sms_txt')),"target"=>3,"link"=>"NotifyCtrl",'title'=>'Alert'));
                    echo json_encode(array("process"=>"complete"));
                }

                else if($_POST['target']=='active'){
                    $this->db->insert('notifications', array("notification_txt"=>preg_replace('/(\'*)/', '',$this->input->post('sms_txt')),"target"=>1,"link"=>"NotifyCtrl",'title'=>'Alert'));
                    echo json_encode(array("process"=>"complete"));
                }

                else if($_POST['target']=='inactive'){
                    $this->db->insert('notifications', array("notification_txt"=>preg_replace('/(\'*)/', '',$this->input->post('sms_txt')),"target"=>0,"link"=>"NotifyCtrl",'title'=>'Alert'));
                    echo json_encode(array("process"=>"complete"));
                }

                else if($_POST['target']=='pending'){
                    $this->db->insert('notifications', array("notification_txt"=>preg_replace('/(\'*)/', '',$this->input->post('sms_txt')),"target"=>2,"link"=>"NotifyCtrl",'title'=>'Alert'));
                    echo json_encode(array("process"=>"complete"));
                }
            }
            

            
        }
    }


}