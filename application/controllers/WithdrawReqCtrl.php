<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class WithdrawReqCtrl extends CI_Controller
{

    public function index()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        $this->load->model('WithdrawReq_m');
        $w_req = $this->WithdrawReq_m->get_withdraw_req(0); // status 0 for reqested withdraw
        $w_his = $this->WithdrawReq_m->get_withdraw_his();
        $w_total = $this->WithdrawReq_m->total_wid_req();

        $data['withdraw_req'] = $w_req;
        $data['withdraw_his'] = $w_his;
        $data['w_total'] = $w_total;

        $this->load->view('templates/1_head.php');
        $this->load->view('templates/2_nav.php');
        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('withdraw_rqst', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');
        $this->load->view('templates/6_script_end.php');
    }

    public function acpt_withdr_req()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        if (isset($_POST['snd_num'])) {

            $this->load->model('WithdrawReq_m');
            $this->load->library('Sms_send');

            $snd_num = $this->input->post('snd_num');
            $user_id = $this->input->post('user_id');
            $id = $this->input->post('id');
            $user_num = $this->input->post('user_num');
            $rcv_num = $this->input->post('rcv_num');
            $tran_id = $this->input->post('tran_id');
            $job_wrk_amnt = $this->input->post('j_amnt');
            $ref_com_amnt = $this->input->post('ref_com_amnt');
            $ref_bon_amnt = $this->input->post('ref_bon_amnt');
            $otr_bon_amnt = $this->input->post('otr_bon_amnt');
            $tot_amnt = $this->input->post('tot_amnt');

            $this->WithdrawReq_m->send_withdraw($snd_num, $tran_id, $tot_amnt, $job_wrk_amnt, $ref_com_amnt, $ref_bon_amnt, $otr_bon_amnt, $id);
            $this->WithdrawReq_m->update_total_bal($ref_bon_amnt, $job_wrk_amnt, $ref_com_amnt, $otr_bon_amnt, $user_id);
            $this->sms_send->send_sms($user_num, "Dear Biz-Bazar User, Your Cash '$tot_amnt' BDT (2% bKash fee charged) has been successfully sent in '$rcv_num', from this Number '$snd_num' and Transection ID is '$tran_id' ,Thank You.");

            $this->session->set_flashdata(array("title" => "Success", "text" => "Amount sent in members account"));
            redirect("/WithdrawReqCtrl");
        }
    }

    public function reject_req()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        if (isset($_POST['id'])) {
            $this->load->library('Sms_send');
            $user_num = $this->input->post('user_num');
            $cuz = $this->input->post('cuz');

            $this->load->model('WithdrawReq_m');
            $this->WithdrawReq_m->reject_req($this->input->post('id'));
            $this->sms_send->send_sms($user_num, $cuz);
            echo json_encode(array('success' => 1));
        }
    }

    public function withdraw_histry()
    {
        $this->load->model('WithdrawReq_m');
        $all_withdraw = $this->WithdrawReq_m->get_all_withdraw($this->input->get('user_id'));
        $total_withdraw = $this->WithdrawReq_m->total_withdrawn($this->input->get('user_id'));

        $data['total_withdraw'] = $total_withdraw;
        $data['all_withdraw'] = $all_withdraw;

        $this->load->view('templates/1_head.php');
        $this->load->view('templates/2_nav.php');
        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('withdraw_history', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');
        $this->load->view('templates/6_script_end.php');
    }
}
