<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

class BalanceCtrl extends CI_Controller
{

    public function index()
    {
        isset($_SESSION['sec_rf_admin']) ? $_SESSION['sec_rf_admin'] == true ? null : redirect('/') : redirect('/');

        $this->load->model('Balance_m');
        $data['users_bal'] = $this->Balance_m->get_users_total_bal();
        $data['deduct_his'] = $this->Balance_m->get_deduct_his();

        $this->load->view('templates/1_head.php');

        $this->load->view('templates/2_nav.php');

        $this->load->view('templates/3_sidebar_menu_left.php');

        $this->load->view('user_balance', $data); //4_dynamic

        $this->load->view('templates/5_footer.php');

        $this->load->view('templates/6_script_end.php');
    }

    public function deduct_amount()
    {
        if (isset($_POST['mob'])) {
            $this->load->model('Balance_m');
            $this->load->model('Notify_m');
            $this->load->library('Sms_send');

            $this->Balance_m->deduct_user_balance($this->input->post('refer_bon_amnt'),$this->input->post('refer_work_amnt'),$this->input->post('work_amnt'),$this->input->post('other_amnt'),$this->input->post('reason'),$this->input->post('user_id'));
            $this->sms_send->send_sms($this->input->post('mob'),$this->input->post('s_txt'));
            $this->Notify_m->set_notify_user($this->input->post('reason'),$this->input->post('s_txt'),'ReportsCtrl/balance_deduct',$this->input->post('user_id'),'<div class="preview-icon bg-info"><i class="mdi mdi-coin"></i></div>');
           
            echo json_encode(array('success'=>1,'refer_bon_amnt'=>$this->input->post('refer_bon_amnt'),'refer_work_amnt'=>$this->input->post('refer_work_amnt'),'work_amnt'=>$this->input->post('work_amnt'),'other_amnt'=>$this->input->post('other_amnt')));
            //print_r($_POST);
        }

    }

}