<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_m extends CI_Model{

    public function count_active_member()
    {
        $q = $this->db->query("SELECT COUNT(id) as active FROM users_detail WHERE active_status=1");
        return $q->result();
    }

    public function count_inactive_member()
    {
        $q = $this->db->query("SELECT COUNT(id) as inactive FROM users_detail WHERE active_status=0");
        return $q->result();
    }

    public function count_pend_member()
    {
        $q = $this->db->query("SELECT COUNT(id) as pending FROM users_detail WHERE active_status=2");
        return $q->result();
    }

    public function count_member_add($time)
    {
        $q = $this->db->query("SELECT COUNT(id) as total_mem FROM `users_detail` WHERE acc_active_date >= curdate()+'%' - INTERVAL '$time' day AND acc_active_date <= CURDATE()+'%' + INTERVAL 1 DAY");
        return $q->result();
    }

    public function total_withdraw($time)
    {
        $q = $this->db->query("SELECT SUM(paid_total_amount) as withdraw FROM withdraw WHERE status=1 AND updated_at >= curdate()+'%' - INTERVAL '$time' day AND updated_at <= CURDATE()+'%' + INTERVAL 1 DAY");
        return $q->result();
    }

    public function user_earned()
    {
        $q = $this->db->query("SELECT SUM(refer_earn_amount) as refer_earn,SUM(work_earn_amount) as work_earn,SUM(refer_work_earn_amount) as refer_work_earn,SUM(other_earn_amount) as other_earn,SUM(refer_earn_amount+`work_earn_amount`+`refer_work_earn_amount`+`other_earn_amount`) as total_earn FROM `total_balance`");
        return $q->result();
    }

    public function top_5_refer()
    {
        $q = $this->db->query("SELECT a.id,a.name,a.picture, b.total_refered from users_detail a , user_lvl b WHERE b.user_id = a.id ORDER BY b.total_refered DESC LIMIT 5");
        return $q->result();
    }
    public function total_reg_fee_added($time)
    {
        $q = $this->db->query("SELECT SUM(amount) as total FROM `activation_reqst` WHERE status=1 and update_date >= curdate()+'%' - INTERVAL '$time' day AND update_date <= CURDATE()+'%' + INTERVAL 1 DAY");
        $r= $q->result();
        return $r[0]; 
    }

}