<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class WithdrawReq_m extends CI_Model

{

    public function get_withdraw_req($status)

    {

        $q = $this->db->query("SELECT b.id,d.mobile_num,b.user_id,a.name ,a.picture,b.receive_number, b.amount,(ceil((b.amount *2/100)/10)*10) as bkash_fee, b.pay_method, b.created_at, (c.refer_earn_amount+c.work_earn_amount+c.refer_work_earn_amount+c.other_earn_amount) as user_total_bal, c.refer_earn_amount,c.work_earn_amount,c.refer_work_earn_amount,c.other_earn_amount FROM users_detail a, withdraw b,total_balance c , users_login d WHERE a.id = b.user_id AND c.user_id=b.user_id AND d.user_id=b.user_id AND b.status ='$status' ORDER by b.id asc");

        return $q->result() ;

    }

    public function total_wid_req()

    {

        $q = $this->db->query("SELECT SUM(amount) as total FROM `withdraw` WHERE status=0");

        $r= $q->result() ;
        return $r[0];
    }



    public function send_withdraw($sendr_num,$tran_id,$paid_amnt,$wrk_amnt,$refer_wrk_amnt,$refer_bon_amnt,$otr_amnt,$id)

    {

        $q = $this->db->query("UPDATE `withdraw` SET`admin_snd_num`='$sendr_num',`transection_id`='$tran_id',`paid_total_amount`='$paid_amnt',`paid_work_amnt`='$wrk_amnt',`paid_refer_work_amnt`='$refer_wrk_amnt',`paid_refer_bon_amnt`='$refer_bon_amnt',`paid_other_amnt`='$otr_amnt',`status`=1,`updated_at`= CURRENT_TIMESTAMP WHERE id='$id'");

    }



    public function update_total_bal($ref_bon_amnt,$wrk_amnt,$ref_com_amnt,$otr_amnt,$user_id)

    {

        $q = $this->db->query("UPDATE `total_balance` SET `refer_earn_amount`=`refer_earn_amount`-'$ref_bon_amnt',`work_earn_amount`= `work_earn_amount`-'$wrk_amnt',`refer_work_earn_amount`=`refer_work_earn_amount`-'$ref_com_amnt',`other_earn_amount`=`other_earn_amount`-'$otr_amnt',`updated_at`=CURRENT_TIMESTAMP WHERE `user_id` = '$user_id'");

    }

   

    public function reject_req($id)

    {

        $q = $this->db->query("UPDATE `withdraw` SET `status`= 2,`updated_at`= CURRENT_TIMESTAMP WHERE id = '$id'");

    }



    public function get_withdraw_his()

    {

        $q = $this->db->query("SELECT

        b.id,

        d.mobile_num,

        b.user_id,
        b.receive_number,
        b.admin_snd_num,

        b.transection_id,

        b.paid_total_amount,
        b.paid_work_amnt,
        b.paid_refer_work_amnt,
        b.paid_refer_bon_amnt,
        b.paid_other_amnt,

        b.updated_at,

        a.name,

        a.picture,

        b.receive_number,

        b.amount,

        b.pay_method,

        b.created_at,

        (CASE  WHEN b.status = 0 THEN 'Pending'

        WHEN b.status = 1 THEN 'Paid'

        WHEN b.status = 2 THEN 'Rejected' END)as status,

        ( c.refer_earn_amount + c.work_earn_amount + c.refer_work_earn_amount + c.other_earn_amount

        ) AS user_total_bal,

        c.refer_earn_amount,

        c.work_earn_amount,

        c.refer_work_earn_amount,

        c.other_earn_amount

    FROM

        users_detail a,

        withdraw b,

        total_balance c,

        users_login d

    WHERE

        a.id = b.user_id AND c.user_id = b.user_id AND d.user_id = b.user_id AND not b.status = 0
        ORDER BY b.updated_at DESC");

        return $q->result() ;

    }

    public function get_all_withdraw($user_id)

    {

        $qury = $this->db->query("SELECT

        b.id,

        d.mobile_num,

        b.user_id,

        b.admin_snd_num,

        b.transection_id,

        b.paid_total_amount,
        b.paid_work_amnt,
        b.paid_refer_work_amnt,
        b.paid_refer_bon_amnt,
        b.paid_other_amnt,
        
        b.updated_at,

        a.name,

        a.picture,

        b.receive_number,

        b.amount,

        b.pay_method,

        b.created_at,

        (CASE  WHEN b.status = 0 THEN 'Pending'

        WHEN b.status = 1 THEN 'Paid'

        WHEN b.status = 2 THEN 'Rejected' END)as status,

        ( c.refer_earn_amount + c.work_earn_amount + c.refer_work_earn_amount + c.other_earn_amount

        ) AS user_total_bal,

        c.refer_earn_amount,

        c.work_earn_amount,

        c.refer_work_earn_amount,

        c.other_earn_amount

    FROM

        users_detail a,

        withdraw b,

        total_balance c,

        users_login d

    WHERE

        a.id = b.user_id AND c.user_id = b.user_id AND d.user_id = b.user_id AND b.user_id = '$user_id'
        ORDER BY b.created_at DESC");

        return $qury->result();

    }

    public function total_withdrawn($user_id)
    {

        $qury = $this->db->query("SELECT sum(paid_total_amount) as total FROM `withdraw` WHERE user_id = '$user_id'");

        $r= $qury->result();
        return $r[0];
    }

}