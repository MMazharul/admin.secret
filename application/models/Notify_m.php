<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notify_m extends CI_Model

{

    public function __construct()

    {

        $this->load->database();

    }

    public function set_notify_admin($title='',$text,$link,$icon='<div class="preview-icon bg-warning"><h3>!</h3></div>')
    {
      $ci = &get_instance();
    
      $query = $ci->db->insert("notifications",array('icon'=>$icon,'title'=>$title,'notification_txt'=>$text,'link'=>$link,'target'=> 4));
      
    }

    public function set_notify_user($title='',$text,$link,$user_id,$icon='<div class="preview-icon bg-warning"><h3>!</h3></div>')
    {
      $ci = &get_instance();
    
      $query = $ci->db->insert("notifications",array('icon'=>$icon,'title'=>$title,'notification_txt'=>$text,'link'=>$link,'user_id'=> $user_id));
      
    }

    public function seen_notify($notification_id)
    {
    
      $query = $this->db->query("UPDATE `notifications` SET `viewed`= 1 WHERE id = '$notification_id'");
      
      
    }

    public function seen_all_notify()
    {
    
      $query = $this->db->query("UPDATE `notifications` SET `viewed`=1 WHERE target = 4");
      
      
    }


}