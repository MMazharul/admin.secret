<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Business_level_m extends CI_Model{

    public function get_all_levels()
    {
        $query = $this->db->query("SELECT * FROM `refer_earn_plan_distribution`");

        return $query->result();
    }

    public function update_level($id,$refer,$percent)
    {
        $query = $this->db->query("UPDATE `refer_earn_plan_distribution` SET`total_refer_member`='$refer',`percentage`='$percent' WHERE id ='$id'");
    }

    public function get_join_fee()
    {
        $query = $this->db->query("SELECT * FROM `join_fee_income` WHERE id=1");

        return $query->result();
    }

    public function update_join_fee($id,$mobile,$fee,$type,$fst,$sec,$trd,$for,$fif)
    {
        $query = $this->db->query("UPDATE `join_fee_income` SET `mobile`='$mobile', `join_fee`='$fee',`pay_type`='$type',`referer_earn_percentage`='$fst',`2nd_lvl_percentage`='$sec',`3rd_lvl_percentage`='$trd',`4th_lvl_percentage`='$for',`5th_lvl_percentage`='$fif' WHERE id = '$id'");
    }

    public function update_earn_per_link($amnt)
    {
        $query = $this->db->query("UPDATE `work_earn_plan_distribution` SET `earn_per_link`= '$amnt' WHERE id=1");
    }

    public function update_withdraw_allow($status)
    {
        $query = $this->db->query("UPDATE `config` SET `withdraw_allow`= '$status' WHERE id=1");
    }
    public function get_withdraw_allow_status()
    {
        $query = $this->db->query("SELECT `withdraw_allow` FROM `config`");

        $r = $query->result();
        return $r[0];
    }

    public function get_earn_perlink()
    {
        $query = $this->db->query("SELECT id, `earn_per_link` FROM `work_earn_plan_distribution`");

        $r = $query->result();
        return $r[0];
    }

}
