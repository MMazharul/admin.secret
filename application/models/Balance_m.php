<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Balance_m extends CI_Model{

    public function get_users_total_bal()
    {
        $q = $this->db->query("SELECT a.name,a.picture,c.mobile_num,b.id,b.user_id,b.refer_earn_amount,b.work_earn_amount,b.refer_work_earn_amount,b.other_earn_amount FROM users_detail a, total_balance b,users_login c WHERE a.id = b.user_id and b.user_id=c.user_id");
        return $q->result();
    }
    public function get_deduct_his()
    {
        $q = $this->db->query("SELECT a.name,a.picture,b.user_id,b.`refer_bonus`,b.refer_work,b.work,b.other,b.reason,b.created_at FROM `balance_deduct`b,users_detail a WHERE a.id=b.user_id ORDER BY created_at DESC");
        return $q->result();
    }

    public function deduct_user_balance($refer_earn,$refer_work,$work,$other,$reason,$user_id)
    {
        $this->db->query("UPDATE `total_balance` SET`refer_earn_amount`= `refer_earn_amount` -'$refer_earn',`work_earn_amount`=`work_earn_amount`-'$work',`refer_work_earn_amount`=`refer_work_earn_amount`-'$refer_work',`other_earn_amount`=`other_earn_amount`-'$other' WHERE user_id='$user_id' ");
        $this->db->query("INSERT INTO `balance_deduct`( `user_id`, `refer_bonus`, `refer_work`, `work`, `other`, `reason`) VALUES ('$user_id','$refer_earn','$refer_work','$work','$other','$reason')"); 
    }
}