<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class JobLimit_m extends CI_Model{

    public function get_limited_acc()
    {
        $q = $this->db->query("SELECT a.id,a.user_id,b.name,b.picture,a.job_limit,IFNULL(DATEDIFF(CURRENT_TIMESTAMP,c.last_refer_date),DATEDIFF(CURRENT_TIMESTAMP,b.acc_active_date)) last_refer FROM `job_limit` a,users_detail b,users_login c WHERE a.user_id=b.id and a.user_id=c.user_id ORDER BY `last_refer` DESC");
        return $q->result();
    }

    public function get_non_limited_acc()
    {
        $q = $this->db->query("SELECT a.id, a.name,a.picture,IFNULL(DATEDIFF(CURRENT_TIMESTAMP,b.last_refer_date),(DATEDIFF(CURRENT_TIMESTAMP,a.acc_active_date))) last_refer FROM users_detail a, users_login b WHERE a.active_status=1 and a.id=b.user_id and a.id NOT IN (SELECT user_id FROM job_limit) ORDER BY `last_refer` DESC");
        return $q->result();
    }

    public function set_user_limit($user_id,$set_limit)
    {
        $q = $this->db->query("INSERT INTO `job_limit`(`user_id`, `job_limit`) VALUES ('$user_id','$set_limit')");
    }

    public function edit_user_limit($user_id,$edit_limit)
    {
        $q = $this->db->query("UPDATE `job_limit` SET `job_limit`= '$edit_limit' WHERE user_id ='$user_id'");
    }

    public function delete_limit($user_id)
    {
        $q = $this->db->query("DELETE FROM `job_limit` WHERE user_id = '$user_id'");
	}
	
	public function mass_remove_limit($min_day){
        $q = $this->db->query("SELECT a.id,a.user_id,b.name,b.picture,a.job_limit,IFNULL(DATEDIFF(CURRENT_TIMESTAMP,c.last_refer_date),DATEDIFF(CURRENT_TIMESTAMP,b.acc_active_date)) last_refer FROM `job_limit` a,users_detail b,users_login c WHERE a.user_id=b.id and a.user_id=c.user_id HAVING last_refer< '$min_day' ORDER BY last_refer DESC");
          return $q->result();
    }
    public function mass_add_limit($min_ref){
        $q = $this->db->query("SELECT user_id FROM user_lvl WHERE total_refered ='$min_ref' and user_id not in  (SELECT user_id from job_limit )");
          return $q->result();
    }
}
