
<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-unfold-more-horizontal"></i>
		</span>
		Job Limited Accounts
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card shadow p-0">
		<div class="card border10">
			<div class="card-body p-3">
				<h4 class="card-title">Limit Job Acc List</h4>
				<div class="table-responsive">
					<table class="table display datatable" id="" >
						<thead>
							<tr>
								<th>#</th>
								<th>
									Name
								</th>
								<th>
									Limit Job
								</th>
								<th>
									Last Refered
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($limited_acc as $acc):?>
							<tr>
							<td><?=$serial++?></td>
							<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$acc->user_id?>" target="_blank"> <?= $acc->name ?></a></td>
							<td><?=$acc->job_limit?></td>
							<td><?=$acc->last_refer?> Days Ago</td>
							<td>
							<button onclick="set_data_edit(this)" id="<?=$acc->user_id?>" data-name="<?=$acc->name?>" data-limit="<?=$acc->job_limit?>" data-toggle="modal" data-target="#editLimitModal" class='btn btn-sm btn-rounded btn-inverse-secondary'><i class="mdi text-dark mdi-pencil"></i></button>
							<a href="<?=base_url('JobLimitCtrl/delete_limit?user_id='.$acc->user_id)?>" onclick="return confirm('Are You Sure Remove <?=$acc->name?> from Limit List ??')" class="btn btn-sm btn-rounded btn-inverse-danger"><i class="mdi mdi-close"></i></a>
							</td>
						
							</tr>
							<?php endforeach?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>




<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-unfold-less-vertical"></i>
		</span>
			Non Job-Limited Members
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card shadow p-0">
		<div class="card border10">
			<div class="card-body p-3">
				<h4 class="card-title">Non Job Limited Members</h4>
				<div class="table-responsive">
					<table class="table display datatable" id="" >
						<thead>
							<tr>
								<th>#</th>
								<th>
									Name
								</th>
								<th>
									Last Refered
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($non_limited_acc as $acc):?>
							<tr>
							<td><?=$serial++?></td>
							<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$acc->id?>" target="_blank"><img src="<?=main_url().'/app-assets/images/members/'.$acc->picture?>" alt="">  <?= $acc->name ?></a></td>
							<td><?=$acc->last_refer?> Days Ago</td>
							<td>
								<button onclick="set_data(this)" id="<?=$acc->id?>" data-name="<?=$acc->name?>" data-toggle="modal" data-target="#setLimitModal" class='btn btn-sm btn-rounded btn-inverse-info'><i class=" mdi  mdi-elevation-decline"></i>Limit</button>
							</td>
						
							</tr>
							<?php endforeach?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- //edit LIMIT modal -->
<div class="modal fade" id="editLimitModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"> <i class="mdi mdi-coin"></i>Edit Job Limit For <span id="u_name"></span></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

	 	<input id="u_id" type="hidden">
	 	
		<h6><i class="mdi  mdi-elevation-decline"></i>Job Limit</h6>
		<input id="e_limit" type="number" value="" class="form-control">

		<br>
		<div id="b_result" class="text-center"></div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button onclick="edit_limit()" type="button" class="btn btn-info">Save <i class="mdi mdi-sync mdi-spin loading"></i> </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>


<!-- //set limit modal -->
<div class="modal fade" id="setLimitModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"> <i class="mdi mdi-coin"></i>Set Job Limit For <span id="u_name"></span></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

	 	<input id="u_id" type="hidden">
	 	
		<h6><i class="mdi  mdi-elevation-decline"></i>Job Limit</h6>
		<input id="limit" type="number" value="" class="form-control">

		<br>
		<div id="b_result" class="text-center"></div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button onclick="set_limit()" type="button" class="btn btn-info">Save <i class="mdi mdi-sync mdi-spin loading"></i> </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>


<script>
let btn =null;
function set_data(e){
	btn = e;
	$('#u_id').val(e.id);
	$('#u_name').html($(e).data('name'));
}
function set_data_edit(e){
	btn = e;
	$('#u_id').val(e.id);
	$('#u_name').html($(e).data('name'));
	$('#e_limit').val($(e).data('limit'));
}
function set_limit(){
	let u_id = $('#u_id').val();
	let limit = $('#limit').val();

	let data= {
		u_id:u_id,
		limit:limit
	};

	if(u_id !==null && !limit !== null){
		$('.loading').show();
		$.ajax({
			type: "post",
			url: "<?=base_url()?>JobLimitCtrl/set_limit",
			data: data,
			dataType: "json",
			success: function (response) {
				if(response.success ==1){
					$('.modal').modal('hide');
					btn.remove();
					$('.loading').hide();
				}
			}
		});
	}
}

function edit_limit(){
	let u_id = $('#u_id').val();
	let limit = $('#e_limit').val();

	let data= {
		u_id:u_id,
		limit:limit
	};

	if(u_id !==null && !limit !== null){
		$('.loading').show();
		$.ajax({
			type: "post",
			url: "<?=base_url()?>JobLimitCtrl/edit_limit",
			data: data,
			dataType: "json",
			success: function (response) {
				if(response.success ==1){
					$('.modal').modal('hide');
					$('.loading').hide();
					btn.remove();
				}
			}
		});
	}
}
</script>