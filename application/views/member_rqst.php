<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-account-convert"></i>
		</span>
		Member Request
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row">
	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">All Requestes</h4>
				<div class="table-responsive">
					<table id="datatable" class="table dataTable display">
						<thead>
							<tr>
								<th>
									Serial
								</th>
								<th>
									Name
								</th>
								<th>
									Amount
								</th>
								<th>
									Sender Number
								</th>
								<th>
									Transection ID
								</th>
								<th>
									Requested At
								</th>
								<th>
									Actions
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($member_reqsts as $member_reqst):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><?= $member_reqst->name ?></td>
								<td><?= $member_reqst->join_fee ?></td>
								<td><?= $member_reqst->sender_num ?></td>
								<td><?= $member_reqst->transection_id ?></td>
								<td><?= nice_date($member_reqst->reqst_date, 'd-M-y') ?></td>
								<td>
									<button onclick="accpt_mem(this);" data-user_id="<?=$member_reqst->user_id?>" data-referer_refer_id="<?=$member_reqst->refer_from?>" data-mob="<?=$member_reqst->mobile_num?>" data-id="<?=$member_reqst->id?>" class='btn btn-sm btn-rounded btn-gradient-success'><i class="mdi mdi-check"></i> <i class="mdi mdi-sync mdi-spin a_ldr"></i> </button>
									&nbsp &nbsp<button onclick=" reject_mem(this)" data-user_id="<?=$member_reqst->user_id?>" data-id="<?=$member_reqst->id?>" class='btn btn-sm btn-rounded btn-gradient-danger'><i class="mdi mdi-close"></i><i class="mdi mdi-sync mdi-spin r_ldr"></i></button>
								</td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-history"></i>
		</span>
		Member Activation History
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row">
	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">All Requestes History</h4>
				<div class="table-responsive">
					<table id="datatable" class="table dataTable display">
						<thead>
							<tr>
								<th>
									Serial
								</th>
								<th>
									Name
								</th>
								<th>
									Amount
								</th>
								<th>
									Sender Number
								</th>
								<th>
									Transection ID
								</th>
								<th>
									Requested At
								</th>
								<th>
									Checked At
								</th>
								<th>
									Status
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($member_reqsts_his as $member_reqst):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><?= $member_reqst->name ?></td>
								<td><?= $member_reqst->join_fee ?></td>
								<td><?= $member_reqst->sender_num ?></td>
								<td><?= $member_reqst->transection_id ?></td>
								<td><?= nice_date($member_reqst->reqst_date, 'd-M-y') ?></td>
								<td><?= nice_date($member_reqst->update_date, 'd-M-y') ?></td>
								<td>
									<span class="badge <?php if($member_reqst->status == 'Pending'){echo 'badge-warning';}elseif($member_reqst->status == 'Activated'){echo 'badge-success';}else{echo 'badge-danger';}?>"><?=$member_reqst->status?></span>
								</td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>



<script>
	$(document).ready(function () {
		$('#datatable').DataTable();
		$(".a_ldr,.r_ldr").hide();
	});

	function accpt_mem(e) {
		let user_id = $(e).data('user_id');
			let referer_refer_id = $(e).data('referer_refer_id');
			let mob = $(e).data('mob');
			let id = $(e).data('id');
			let data = {
				activate:true,
				user_id: user_id,
				referer_refer_id: referer_refer_id,
				mob: mob,
				id: id
			};
		if (confirm('Are You sure to Accept ?')) {
			$(".a_ldr").show();
			$.ajax({
				type: "post",
				url: "<?=base_url('MemberCtrl/member_activation')?>",
				data: data,
				dataType: 'json',
				success: function (response) {
					console.log(response)
					if(response.success==1){
						location.reload();
					}
					else{ location.reload();
						}
				},
				error:function () {
					 location.reload();
				}
			});
		}

	}

	function reject_mem(e) {
		let user_id = $(e).data('user_id');
			let id = $(e).data('id');
			let data = {
				user_id: user_id,
				id: id
			};
		if (confirm('Are You sure to Decline ?')) {
			$(".r_ldr").show();
			$.ajax({
				type: "post",
				url: "<?=base_url('MemberCtrl/member_decline')?>",
				data: data,
				dataType: 'json',
				success: function (response) {
					if(response.success==1){
						location.reload();
					}
					else{ location.reload();}
				},
				error:function () {
					location.reload();
				}
			});
		}

	}
</script>
