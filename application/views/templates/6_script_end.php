  <script>

  const user_id = "<?= $this->session->user_id ?>";


  var $loading = $('.loading');
    $loading.hide();
    //Attach the event handler to any element
    $(document).ajaxStart(function () {
    		//ajax request went so show the loading image
        $loading.show();
        $("button,a").attr("disabled",true).css('pointer-events','none');
    	})
    	.ajaxStop(function () {
    	
        $loading.hide();
        $("button,a").attr("disabled",false).css('pointer-events','auto');
    	});
  </script>

  <!-- plugins:js -->

  <script src="<?= base_url() ?>app-assets/vendors/js/vendor.bundle.base.js"></script>

  <script src="<?= base_url() ?>app-assets/vendors/js/vendor.bundle.addons.js"></script>

  <script src="<?= base_url() ?>app-assets/js/moment.min.js"></script>
  <script src="<?= base_url() ?>app-assets/js/daterangepicker.min.js"></script>

 
  <!-- endinject -->

  <!-- Plugin js for this page-->

  <!-- End plugin js for this page-->

  <!-- inject:js -->

  <script src="<?= base_url() ?>app-assets/js/off-canvas.js"></script>

  <script src="<?= base_url() ?>app-assets/js/misc.js"></script>

  <!-- endinject -->

  <!-- Custom js for this page-->

  <script src="<?= base_url() ?>app-assets/js/dashboard.js"></script>

  <script src="<?= base_url() ?>app-assets/js/datatable.js"></script>

  <script src="<?= base_url() ?>app-assets/js/ajax/activation.js"></script>

  <!-- End custom js for this page-->

  <script>
	$(document).ready(function () {
		$('#datatable,.datatable').DataTable();
	});


  function all_notify_read() {
      if(confirm("Mark All Notification as Read ?")){
        $.ajax({
          type: "post",
          url: "<?=base_url('NotifyCtrl/markall_notify_read')?>",
          data: "data",
          dataType: "json",
          success: function (response) {
            if(response.success==1){
                $(".dropdown-item").removeClass('alert-info');
                $(".count-symbol").hide();  
            }
          }
        });
      }
    }
  
   </script>

    
<script src="<?=base_url('app-assets/js/dataTable.button.js')?>"></script>
  <script src="<?=base_url('app-assets/js/dataTable.print.js')?>"></script>

 <script>

    <?php if($this->session->flashdata('title')!==null){ ?>

         $(document).ready(function () {

          VanillaToasts.create({

          title:"<?php echo $this->session->flashdata('title') ?>",

          text:"<?php echo $this->session->flashdata('text') ?>",

           type:"info",

          //icon:"<?//=base_url()?>///app-assets/images/logo.gif",

          timeout:5000

             });

         });



     <?php } ?>

    //console.log(moment('09-Jan-19 -- 12:0:00 pm','DD-MMM-YY -- h:m:ss a').format('YYYYMMDDHms'));
     /* datatable search */
		/* $.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {

			var from = parseInt( moment($('#from').html(),'DD-MMM-YY -- h:m:ss a').format('YYYYMMDDHms'), 10 );
      //console.log(from);
			var to = parseInt( moment($('#to').html(),'DD-MMM-YY -- h:m:ss a').format('YYYYMMDDHms'), 10 );
			var date =  parseInt( moment(data[4],'DD-MMM-YY -- h:m:ss a').format('YYYYMMDDHms'), 10 ) || 0; // use data for the age column
	//console.log(date);
			if ( ( isNaN( to ) && isNaN( from ) ) ||
				( isNaN( to ) && date <= from ) ||
				( to <= date   && isNaN( from ) ) ||
				( to <= date   && date <= from ) )
			{
				return true;
			}
			return false;
		}
	);
	
	$(document).ready(function() {
		var table = $('#search_datatable').DataTable();
		 */
		// Event listener to the two range filtering inputs to redraw on input
		/* $('#from, #to').bind("DOMSubtreeModified",function(){
      console.log(213123);
			table.draw();
		} ); 

  
	} );*/

  </script>

</body>



</html>
