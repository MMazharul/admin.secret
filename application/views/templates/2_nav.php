<?php $notifications = notifications();
$notify_count = notify_count()?>

<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
	<div class="text-center  navbar-brand-wrapper d-flex align-items-center justify-content-center">
		<a class="navbar-brand brand-logo" href="index.html"><img id="icon" src="<?= base_url() ?>app-assets/images/logo.gif" alt="logo" /><img id="text" src="<?= base_url() ?>app-assets/images/quick-earn.png" alt="logo" /></a>
		<a class="navbar-brand brand-logo-mini" href="index.html"><img src="<?= base_url() ?>app-assets/images/logo.gif"
			 alt="logo" /></a>
	</div>
	<div class="navbar-menu-wrapper d-flex align-items-stretch">
		<ul class="navbar-nav navbar-nav-right">
			
			<li class="nav-item d-none d-lg-block full-screen-link">
				<a class="nav-link">
					<i class="mdi mdi-fullscreen" id="fullscreen-button"></i>
				</a>
			</li>
			
			<li class="nav-item dropdown">
				<a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
					<i class="mdi mdi-bell-outline"></i>
					<?=$notify_count > 0 ? '<span class="count-symbol bg-danger"></span>' : "" ?>
				</a>
				<div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
				<h6 class="p-3 mb-0">Notifications<small class="float-right"><a onclick="all_notify_read()" href="#" class="text-gray"><i class="mdi mdi-eye-outline"></i> mark all as read</a></small></h6>
					<div class="dropdown-divider"></div>
					<?php foreach($notifications as $notify):?>
					<a href="<?=base_url().$notify->link.'?notify_id='.$notify->id?>" class="dropdown-item preview-item <?=$notify->viewed == 0 ? "alert-info" : ""?>">
						<div class="preview-thumbnail">
								<?=$notify->icon?>
						</div>
						<div class="preview-item-content d-flex align-items-start flex-column justify-content-center">
							<div style="width:100%"><h6 class="preview-subject font-weight-normal mb-1"><?=$notify->title?>&nbsp<small class="float-right text-secondary"><time datetime="<?=$notify->created_at?>"><?=time_ago($notify->created_at)?></time></small></h6></div>
							<p class="text-gray ellipsis mb-0">
								<?=$notify->notification_txt?>
							</p>
						</div>
					</a>
					<div class="dropdown-divider"></div>
					<?php endforeach ?>
					
					<a href="<?=base_url("NotifyCtrl")?>"><h6 class="p-3 mb-0 text-center">See all notifications</h6></a>
				</div>
			</li>
			<li class="nav-item nav-profile dropdown">
				<a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
					<div class="nav-profile-img">
						<img src="<?= base_url() ?>app-assets/images/faces-clipart/pic-4.png" alt="image">
						<span class="availability-status online"></span>
					</div>
					<div class="nav-profile-text">
						<p class="mb-1 text-black">QC-Admin</p>
					</div>
				</a>
				<div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
					<a class="dropdown-item" href="#">
						<i class="mdi mdi-cached mr-2 text-success"></i>
						Activity Log
					</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="<?=base_url()?>index.php/LoginCtrl/logout">
						<i class="mdi mdi-logout mr-2 text-primary"></i>
						Signout
					</a>
				</div>
			</li>
			
		</ul>
		<button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
			<span class="mdi mdi-menu"></span>
		</button>
	</div>
</nav>


<div class="container-fluid page-body-wrapper">
