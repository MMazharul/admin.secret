

<nav class="sidebar sidebar-offcanvas" id="sidebar">

	<ul class="nav">

		<li class="nav-item nav-profile">

			<a href="#" class="nav-link">

				<div class="nav-profile-image">

					<img src="<?= base_url() ?>app-assets/images/faces-clipart/pic-4.png" alt="profile">

					<span class="login-status online"></span>

					<!--change to offline or busy as needed-->

				</div>

				<div class="nav-profile-text d-flex flex-column">

					<span class="font-weight-bold mb-2">QC-Admin</span>

					<span class="text-secondary text-small">Admin</span>

				</div>

				<i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>

			</a>

		</li>

		<li class="nav-item">

			<a class="nav-link" href="<?=base_url()?>DashboardCtrl">

				<span class="menu-title">Dasdhboard</span>

				<i class="mdi mdi-home menu-icon"></i>

			</a>

		</li>

		<li class="nav-item">

			<a class="nav-link" href="<?=base_url()?>JobLinkCtrl">

				<span class="menu-title">Jobs Link</span>

				<i class="mdi mdi-link menu-icon"></i>

			</a>

		</li>

		<li class="nav-item">

			<a class="nav-link" href="<?=base_url()?>MemberCtrl/all_members">

				<span class="menu-title">Members</span>

				<i class="mdi mdi-account menu-icon"></i>

			</a>

		</li>

		<li class="nav-item">

			<a class="nav-link" href="<?=base_url()?>MemberCtrl/member_rqst">

				<span class="menu-title">Member Request <?=!total_activ_req_count()==null ? '<span class="badge badge-pill badge-gradient-danger">'.total_activ_req_count().'</span>':null?>  </span>

				<i class="mdi mdi-account-convert menu-icon"></i>

			</a>

		</li>

		<li class="nav-item">

		<a class="nav-link" href="<?=base_url()?>MemberCtrl/refer_network">

			<span class="menu-title">Refer Network</span>

			<i class="mdi mdi-share-variant menu-icon"></i>

		</a>

		</li>

		<li class="nav-item">

			<a class="nav-link" href="<?=base_url()?>WithdrawReqCtrl">

				<span class="menu-title">Withdraw Request <?=!total_withdraw_count()==null ? '<span class="badge badge-pill badge-gradient-danger">'.total_withdraw_count().'</span>':null?> </span>

				<i class="mdi mdi-unfold-less-horizontal menu-icon"></i>

			</a>

		</li>

		<li class="nav-item">

			<a class="nav-link" href="<?=base_url('FlexiloadCtrl/load_fund_req')?>">

				<span class="menu-title">Fund Request <?=!total_fund_req_count()==null ? '<span class="badge badge-pill badge-gradient-danger">'.total_fund_req_count().'</span>':null?> </span>

				<i class="mdi mdi-unfold-less-horizontal menu-icon"></i>

			</a>

		</li>



		<li class="nav-item">
			<a class="nav-link" data-toggle="collapse" href="#mc" aria-expanded="false" aria-controls="ui-basic">
				<span class="menu-title">Matured Account</span>
				<i class="menu-arrow"></i>
				<i class="mdi mdi-chart-line  menu-icon"></i>
			</a>
			<div class="collapse" id="mc">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>MemberCtrl/matured_acc">Total (500+) </a></li>
					<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>MemberCtrl/matured_acc_job">Job (500+) </a></li>
					<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>MemberCtrl/matured_acc_refer">Refer (500+) </a></li>
					
				</ul>
			</div>
		</li>


		<li class="nav-item">

			<a class="nav-link" href="<?=base_url()?>BusinessLvlCtrl">

				<span class="menu-title">Business Plan</span>

				<i class="mdi mdi-map menu-icon"></i>

			</a>

		</li>

		<li class="nav-item">
			<a class="nav-link" data-toggle="collapse" href="#joblimit" aria-expanded="false" aria-controls="ui-basic">
				<span class="menu-title">Job Limit</span>
				<i class="menu-arrow"></i>
				<i class="mdi mdi-unfold-less-horizontal  menu-icon"></i>
			</a>
			<div class="collapse" id="joblimit">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>JobLimitCtrl">Job Limited Acc </a></li>
				</ul>
			</div>
		</li>

		<li class="nav-item">
			<a class="nav-link" data-toggle="collapse" href="#reports" aria-expanded="false" aria-controls="ui-basic">
				<span class="menu-title">Reports</span>
				<i class="menu-arrow"></i>
				<i class="mdi mdi-chart-line  menu-icon"></i>
			</a>
			<div class="collapse" id="reports">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>ReportCtrl?from=<?= date('Y-m-d')?> 12:00:00 am&to=<?= date('Y-m-d')?> 11:59:59 pm">Today </a></li>
					<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>ReportCtrl?from=<?= date('Y-m-d', strtotime('-6 days'))?> 12:00:00 am&to=<?= date('Y-m-d')?> 11:59:59 pm">Last 7 Days  </a></li>
					<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>ReportCtrl?from=<?= date('Y-m-d', strtotime('-29 days'))?> 12:00:00 am&to=<?= date('Y-m-d')?> 11:59:59 pm">Last 30 Days  </a></li>
					<li class="nav-item"> <a class="nav-link" href="<?=base_url()?>ReportCtrl">Total</a></li>
				</ul>
			</div>
		</li>

		<li class="nav-item">

			<a class="nav-link" href="<?=base_url()?>BalanceCtrl">

				<span class="menu-title">Users Balance</span>

				<i class="mdi mdi-coin menu-icon"></i>

			</a>

		</li>

		<li class="nav-item">

			<a class="nav-link" href="<?=base_url()?>NoticeCtrl">

				<span class="menu-title">Notices</span>

				<i class="mdi mdi-volume-medium menu-icon"></i>

			</a>

		</li>

		<li class="nav-item">

			<a class="nav-link" href="<?=base_url()?>AlertCtrl">

				<span class="menu-title">Alert</span>

				<i class="mdi mdi-alert-circle-outline menu-icon"></i>

			</a>

		</li>

		<li class="nav-item">

			<a class="nav-link" data-toggle="collapse" href="#flexiload" aria-expanded="false" aria-controls="ui-basic">
				<span class="menu-title">Flexiload Business</span>
				<i class="menu-arrow"></i>

			</a>
			<div class="collapse" id="flexiload">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item"> <a class="nav-link" href="<?=base_url('setting/package_list')?>">Packages</a></li>
					<li class="nav-item"> <a class="nav-link" href="<?=base_url('setting/flexiload_setting')?>">Flexiload Setting</a></li>
				</ul>
			</div>



		</li>




	</ul>

</nav>





<!-- main panel -->

<div class="main-panel">





	<!-- wrappper start -->

	<div class="content-wrapper">
