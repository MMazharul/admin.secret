<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="row">
	<div class="col-3">
		<form>
		<div class="form-group">
			<label class="sr-only" for="inlineFormInputGroup">Account Activation Pin</label>
			<div class="input-group mb-2">
				<div class="input-group-prepend">
					<div class="input-group-text">@</div>
				</div>
				<input type="text" class="form-control" id="ac_pin" placeholder="Pin">
			</div>
		</div>

		<div class="form-group">
			<label class="sr-only" for="inlineFormInputGroup">Sender Number</label>
			<div class="input-group mb-2">
				<div class="input-group-prepend">
					<div class="input-group-text">@</div>
				</div>
				<input type="number" class="form-control" id="sndr_num" placeholder="Sender Number">
			</div>
		</div>

		<div class="form-group">
			<label class="sr-only" for="inlineFormInputGroup">Transection ID</label>
			<div class="input-group mb-2">
				<div class="input-group-prepend">
					<div class="input-group-text">@</div>
				</div>
				<input type="text" class="form-control" id="tran_id" placeholder="Transection ID">
			</div>
		</div>

		<div class="form-group">
			<label class="sr-only" for="inlineFormInputGroup">Transection ID</label>
			<div class="input-group mb-2">
				<button onclick="active_req()" type="button" class="btn btn-primary btn-rounded shadow">Send <i id="loader" style="display:none" class="mdi mdi-loading mdi-spin"></i></button>
			</div>
			<div id="activ_rst"></div>
		</div>
		
		</form>
	</div>
</div>
<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-home"></i>
		</span>
		Dashboard
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

