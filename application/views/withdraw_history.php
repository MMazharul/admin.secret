
<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-danger text-white mr-2">
			<i class="mdi mdi-cash-multiple"></i>
		</span>
		Withdraw History
	</h3>
</div>




<div class="row p-0">
	<div class="col-12 m-0 p-0">
		<div class="card shadow border10 m-0">
			<div class="card-body p-3">
				<h4 class="card-title">All History</h4>
				<h4 class="card-title">Your Total Withdrawn : ৳<?=$total_withdraw->total?></h4>
				<div class="table-responsive">
					<table id="datatable" class="table dataTable display">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Member
								</th>
								<th>
									Status
								</th>
								<th>
									Requested Amount
								</th>
								<th>
									Paid Amount
								</th>
								<th>
									Opration Date
								</th>
								<th>Details</th>
								
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($all_withdraw as $his):?>
							<tr>
								<td>
									<?= $serial++ ?>
								</td>
								<td><?= $his->name ?>
								</td>
								<td><span class="badge <?php if($his->status == "Paid"){ echo "badge-success";} elseif($his->status == "Pending"){ echo "badge-warning";} else{ echo "badge-danger";} ?>"> <?=$his->status?> </span>
								</td>
								<td>
									<span class="badge badge-info">৳ <?= $his->amount?></span>
								</td>
								<td>
									<span class="badge badge-warning text-dark">৳ <?=$his->paid_total_amount?></span><br>
								</td>
								<td>
									<?= nice_date($his->updated_at,"d-M-Y")?>
								</td>
								<td><button onclick="set_data_modal(this)" data-name="<?= $his->name ?>" data-status="<?= $his->status ?>"
								 		data-req-amount="<?= $his->amount ?>" data-paid-amount="<?= $his->paid_total_amount ?>" 
										data-admin-snd-num="<?= $his->admin_snd_num ?>" data-tran-id="<?= $his->transection_id ?>"
										data-checked-at="<?= $his->updated_at ?>" data-requested-at="<?= $his->created_at ?>" 
										data-rcv-num="<?= $his->receive_number ?>" data-pay-method="<?= $his->pay_method ?>"
										data-job-earn="<?= $his->paid_work_amnt ?>" data-refer-work-amnt="<?= $his->paid_refer_work_amnt ?>"
										data-refer-bon-amnt="<?= $his->paid_refer_bon_amnt ?>" data-other_amnt="<?= $his->paid_other_amnt ?>" data-toggle="modal" data-target="#withdraw_details_modal" class="btn btn-sm btn-rounded btn-gradient-dark">Details <i class="mdi mdi-details"></i></button></td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade border10" id="withdraw_details_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Withdraw History Deatils</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
		<div class="row">
			<div class="col-12">
				<div class="mb-2"><b>Name :</b> <span id="name"></span></div>
				<div class="mb-2"><b>Status :</b> <span id="status" class="badge badge-warning"></span></div>
				<div class="mb-2"><b>Receive Number :</b> <span  id="rcv-num" class="badge badge-pill badge-dark"></span></div>
				<div class="mb-2"><b>Admin Send Number :</b> <span  id="admin-snd-num" class="badge badge-pill badge-dark"></span></div>
				<div class="mb-2"><b>Transection ID :</b> <span id="tran-id" ></span></div>
				<div class="mb-2"><b>Pay Method :</b> <span id="pay-method" ></span></div>
				<div class="mb-2"><b>Requested Amount :</b> <span id="req-amount" ></span></div>
				<div class="mb-2"><b>Paid Amount :</b> <span id="paid-amount" ></span></div>
				<div class="mb-2 ml-3"><b>Paid Job Amount :</b> <span id="job-earn" ></span></div>
				<div class="mb-2 ml-3"><b>Paid Refer Commission Amount :</b> <span id="refer-work-amnt" ></span></div>
				<div class="mb-2 ml-3"><b>Paid Refer Bonus Amount :</b> <span id="refer-bon-amnt" ></span></div>
				<div class="mb-2 ml-3"><b>Paid Other Bonus Amount :</b> <span id="other_amnt" ></span></div>
				<hr>
				<div class="mb-2"><b>Requested At :</b> <span id="requested-at" ></span></div>
				<div class="mb-2"><b>Checked At :</b> <span id="checked-at" ></span></div>
			</div>
		</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script>
function set_data_modal(e) { 
	$('#name').html($(e).data('name'));
	$('#status').html($(e).data('status'));
	$('#req-amount').html($(e).data('req-amount'));
	$('#paid-amount').html($(e).data('paid-amount'));
	$('#admin-snd-num').html($(e).data('admin-snd-num'));
	$('#tran-id').html($(e).data('tran-id'));
	$('#checked-at').html($(e).data('checked-at'));
	$('#requested-at').html($(e).data('requested-at'));
	$('#rcv-num').html($(e).data('rcv-num'));
	$('#pay-method').html($(e).data('pay-method'));
	$('#job-earn').html($(e).data('job-earn'));
	$('#refer-work-amnt').html($(e).data('refer-work-amnt'));
	$('#refer-bon-amnt').html($(e).data('refer-bon-amnt'));
	$('#other_amnt').html($(e).data('other_amnt'));
 }
</script>