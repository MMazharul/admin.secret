<style>
	.switch {
		position: relative;
		display: inline-block;
		width: 60px;
		height: 34px;
	}

	.switch input {
		opacity: 0;
		width: 0;
		height: 0;
	}

	.slider {
		position: absolute;
		cursor: pointer;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: #ccc;
		-webkit-transition: .4s;
		transition: .4s;
	}

	.slider:before {
		position: absolute;
		content: "";
		height: 26px;
		width: 26px;
		left: 4px;
		bottom: 4px;
		background-color: white;
		-webkit-transition: .4s;
		transition: .4s;
	}

	input:checked+.slider {
		background-color: #2196F3;
	}
	.slider {
		background-color: #28c11e;
	}

	input:focus+.slider {
		box-shadow: 0 0 1px #2196F3;
	}

	input:checked+.slider:before {
		-webkit-transform: translateX(26px);
		-ms-transform: translateX(26px);
		transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
		border-radius: 34px;	
	}

	.slider.round:before {
		border-radius: 50%;
	}
</style>

<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-account-convert"></i>
		</span>
		Business Plans
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row">
	<div class="col-md-4 col-12 border10 shadow bg-white m-3 text-center p-2">
	Allow Withdraw Options to User
	<br>
	<br>
	<div class="d-flex justify-content-center ">
		<p>Other Withdraw &nbsp</p>
		<label class="switch"> 
		<input id="w_alw" onchange="withdraw_alw()" <?=$w_alw->withdraw_allow ? 'checked' : ''?> type="checkbox">
		<span class="slider round"></span> 
		</label>
		<p>&nbsp Job Withdraw</p>&nbsp <i class="mdi mdi-sync mdi-spin loading"></i>
	</div>
	<div id="rs"></div>

	</div>
</div>


<div class="card shadow mb-3 p-1shadow border10">
	<div class="table-responsive">
		<table class="table" >
			<thead>
				<tr class="text-center">
					<td><h5>Earn Per Link</h5></td>
					<td>৳<input id="earn-val" readonly type="number" class="inline-input" value="<?= $earn_pl->earn_per_link ?>"></td>
					<td>
					<button id="<?=$earn_pl->id?>" onclick="edit_e(this)" class='btn btn-sm btn-rounded btn-inverse-secondary'><i class="text-dark mdi mdi-pencil"></i></button>
					<button data-id="<?=$earn_pl->id?>" data-earn-value="<?= $earn_pl->earn_per_link ?>" onclick="cancel_e(this)" class='btn btn-sm btn-rounded btn-inverse-danger v display-none'><i class="mdi mdi-close"></i></button>
					<button data-id="<?=$earn_pl->id?>" onclick="save_e(this)" class='btn btn-sm btn-rounded btn-inverse-success v display-none'><i class="mdi mdi-content-save"></i></button>
					</td>
					
				</tr>
			</thead>
		</table>
	</div>
</div>

<div class="card shadow mb-3 p-1shadow border10">
	<h3 class="text-center">Join Fee</h3>
	<div class="table-responsive">
		<table class="table" >
			<thead>
				<tr>
					<th><b>Send Money<br>Number</b></th>
					<th><b>Join Fee</b></th>
					<th><b>Pay<br>Method</br></th>
					<th><b>1st Lvl<br>Referer<br>Percentage</b></th>
					<th><b>2nd Lvl<br>Percentage</b></th>
					<th><b>3rd Lvl<br>Percentage</b></th>
					<th><b>4th Lvl<br>Percentage</b></th>
					<th><b>5th Lvl<br>Percentage</b></th>
					<th><b>Actions</b></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><input id="snd_num" readonly class="inline-input" style="width:140px!important" type="number" value="0<?=$join_fee[0]->mobile?>"></td>
					<td>৳<input id="fee-<?=$join_fee[0]->id?>" readonly type="number" class="inline-input" value="<?=$join_fee[0]->join_fee?>"></td>
					<td><input id="type" readonly type="text" class="inline-input" value="<?=$join_fee[0]->pay_type?>" style="width:75px!important"></td>
					<td><input id="referer_percent-<?=$join_fee[0]->id?>" readonly type="number" class="inline-input" value="<?=$join_fee[0]->referer_earn_percentage?>">%</td>
					<td><input id="2nd_referer_percent" readonly type="number" class="inline-input" value="<?=$join_fee[0]->{'2nd_lvl_percentage'}?>">%</td>
					<td><input id="3rd_referer_percent" readonly type="number" class="inline-input" value="<?=$join_fee[0]->{'3rd_lvl_percentage'}?>">%</td>
					<td><input id="4th_referer_percent" readonly type="number" class="inline-input" value="<?=$join_fee[0]->{'4th_lvl_percentage'}?>">%</td>
					<td><input id="5th_referer_percent" readonly type="number" class="inline-input" value="<?=$join_fee[0]->{'5th_lvl_percentage'}?>">%</td>
					<td>
					<button data-join-fee-id="<?=$join_fee[0]->id?>" onclick="edit(this)" class='btn btn-sm btn-rounded btn-inverse-secondary'><i class="text-dark mdi mdi-pencil"></i></button>
					<button data-jf-id="<?=$join_fee[0]->id?>" onclick="cancel_jf(this)" class='btn btn-sm btn-rounded btn-inverse-danger display-none' data-fee-value="<?=$join_fee[0]->join_fee?>" data-type="<?=$join_fee[0]->pay_type?>" data-percent-value="<?=$join_fee[0]->referer_earn_percentage?>" data-2nd-percent-value="<?=$join_fee[0]->{'2nd_lvl_percentage'}?>" data-3rd-percent-value="<?=$join_fee[0]->{'3rd_lvl_percentage'}?>" data-4th-percent-value="<?=$join_fee[0]->{'4th_lvl_percentage'}?>" data-5th-percent-value="<?=$join_fee[0]->{'5th_lvl_percentage'}?>"  data-send_num="0<?=$join_fee[0]->mobile?>" ><i class="mdi mdi-close"></i></button>
					<button data-jf-id="<?=$join_fee[0]->id?>" onclick="save_jf(this)" class='btn btn-sm btn-rounded btn-inverse-success display-none'><i class="mdi mdi-content-save"></i> <i class="mdi mdi-reload mdi-spin loading"></i> </button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h3 class="text-center"><i class="mdi mdi-chart-bar"></i> Levels</h3>
				<div class="table-responsive">
					<table class="table dataTable display">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								<th>
									Total<br>Refer
								</th>
								<th>
									Percentage
								</th>
								<th>
									Actions
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($levels as $key=> $level):?>
							
							<tr>
								<td><?= $serial++ ?></td>
								<td><img src="<?=main_url("app-assets/images/level/".$level->reputation_milestone_icon)?>" class="rounded-0" style="width:auto!important"> &nbsp<b><?= $level->reputation_milestone_name ?></b></td>
								<td><input id="refer-<?=$level->id?>" readonly type="number" class="inline-input" value="<?= $level->total_refer_member ?>"></td>
								<td><input id="percent-<?=$level->id?>" readonly type="number" class="inline-input" value="<?= $level->percentage ?>">%</td>
								<td>
									<button id="<?=$level->id?>" onclick="editable(this)" class='btn btn-sm btn-rounded btn-inverse-secondary'><i class="text-dark mdi mdi-pencil"></i></button>
									<button data-id="<?=$level->id?>" onclick="cancel(this)" class='btn btn-sm btn-rounded btn-inverse-danger display-none' data-refer-value="<?= $level->total_refer_member ?>" data-percent-value="<?= $level->percentage ?>" ><i class="mdi mdi-close"></i></button>
									<button data-id="<?=$level->id?>" onclick="save(this)" class='btn btn-sm btn-rounded btn-inverse-success display-none'><i class="mdi mdi-content-save"></i></button>
								</td>
		
							</tr>
							<?php if($serial===17){?> 
								<tr>
									<td></td>
									<td><h3 class="text-right"><i class="mdi mdi-diamond text-danger"></i> Pro Levels <br><p>Apply for all refer members</p></h3>
									</td>
									<td></td>
									<td></td>
									<td></td>
								</tr> <?php }?>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>



<script>
	$(document).ready(function () {
		$('#datatable').DataTable();
	});

	function editable(e) {

		$(`#refer-${e.id},#percent-${e.id}`).attr('readonly',false).css("border","2px solid #aadbe6");
		$("#"+e.id).css("display","none");
		$(`*[data-id="${e.id}"]`).removeClass("display-none");

	}

	function cancel(e) {

		$("#"+$(e).data("id")).css("display","block");
		$(`*[data-id="${$(e).data("id")}"]`).addClass("display-none");
		$(`#refer-${$(e).data("id")},#percent-${$(e).data("id")}`).attr('readonly',true).css("border","none");
		$("#refer-"+$(e).data("id")).val($(e).data('refer-value'));
		$("#percent-"+$(e).data("id")).val($(e).data('percent-value'));
	}

	function save(e) {

		let id= $(e).data("id");
		let refer= $("#refer-"+$(e).data("id")).val();
		let percent= $("#percent-"+$(e).data("id")).val();

		let data = {
			id : id,
			refer : refer,
			percent : percent
		};

		$.ajax({
			type: "post",
			url: "<?=base_url('/BusinessLvlCtrl/update_level')?>",
			data: data,
			dataType: "json",
			success: function (response) {
				if(response.success == 1){
					$("#"+$(e).data("id")).css("display","block");
					$(`*[data-id="${$(e).data("id")}"]`).addClass("display-none");
					$(`#refer-${$(e).data("id")},#percent-${$(e).data("id")}`).attr('readonly',true).css("border","none");
				}
				else{console.log("error");}
			}
		});
	}

	function edit(e) {
		let id = $(e).data("join-fee-id");
		$(`#fee-${id},#referer_percent-${id},#type,#2nd_referer_percent,#3rd_referer_percent,#4th_referer_percent,#5th_referer_percent,#snd_num`).attr('readonly',false).css("border","2px solid #aadbe6");
		$(e).css("display","none");
		$(`*[data-jf-id="${id}"]`).removeClass("display-none");
	}

	function cancel_jf(e) {
		let id = $(e).data("jf-id");
		$(`*[data-join-fee-id="${id}"]`).css("display","block");
		$(`*[data-jf-id="${id}"]`).addClass("display-none");
		$(`#fee-${id},#referer_percent-${id},#type,#2nd_referer_percent,#3rd_referer_percent,#4th_referer_percent,#5th_referer_percent,#snd_num`).attr('readonly',true).css("border","none");
		$("#fee-"+id).val($(e).data('fee-value'));
		$("#type").val($(e).data('type'));
		$("#referer_percent-"+id).val($(e).data('percent-value'));
		$("#2nd_referer_percent").val($(e).data('2nd-percent-value'));
		$("#3rd_referer_percent").val($(e).data('3rd-percent-value'));
		$("#4th_referer_percent").val($(e).data('4th-percent-value'));
		$("#5th_referer_percent").val($(e).data('5th-percent-value'));
		$("#snd_num").val($(e).data('send_num'));
	}

	function save_jf(e) {
		let id= $(e).data("jf-id");
		let fee= $("#fee-"+id).val();
		let type= $("#type").val();
		let rf_percent= $("#referer_percent-"+id).val();
		let scnd_rf_percent= $("#2nd_referer_percent").val();
		let trd_rf_percent= $("#3rd_referer_percent").val();
		let frth_rf_percent= $("#4th_referer_percent").val();
		let fifth_rf_percent= $("#5th_referer_percent").val();
		let snd_num= $("#snd_num").val();

		let data = {
			id : id,
			fee : fee,
			type : type,
			rf_percent : rf_percent,
			scnd_rf_percent:scnd_rf_percent,
			trd_rf_percent:trd_rf_percent,
			frth_rf_percent:frth_rf_percent,
			fifth_rf_percent:fifth_rf_percent,
			snd_num:snd_num
		};

		$.ajax({
			type: "post",
			url: "<?=base_url('/BusinessLvlCtrl/update_join_fee')?>",
			data: data,
			dataType: "json",
			success: function (response) {
				if(response.success == 1){
					$(`*[data-join-fee-id="${id}"]`).css("display","block");
					$(`*[data-jf-id="${id}"]`).addClass("display-none");
					$(`#fee-${id},#referer_percent-${id},#type,#2nd_referer_percent,#3rd_referer_percent,#4th_referer_percent,#5th_referer_percent,#snd_num`).attr('readonly',true).css("border","none");
				}
				else{console.log("error");}
			}
		});
	}


	function edit_e(e) {
		let id = e.id;
		$(`#earn-val`).attr('readonly',false).css("border","2px solid #aadbe6");
		$(e).css("display","none");
		$(`.v`).removeClass("display-none");
	}

	function cancel_e(e) {
		$("#"+$(e).data("id")).css("display","block");
		$(`.v`).addClass("display-none");
		$(`#earn-val`).attr('readonly',true).css("border","none");
		$("#earn-val").val($(e).data('earn-value'));
	}

	function save_e(e) {
		let id = $(e).data('id');
		let val = $('#earn-val').val();
		let data={
			id:id,
			val:val
		};
		$.ajax({
			type: "post",
			url: "<?=base_url('/BusinessLvlCtrl/update_earn_per_link')?>",
			data: data,
			dataType: "json",
			success: function (response) {
				if(response.success=1){
					$(`#earn-val`).attr('readonly',true).css("border","none");
					$(`.v`).addClass("display-none");
					$("#"+$(e).data("id")).css("display","block");
				}
			}
		});
	}

function withdraw_alw() {

		let allow = document.getElementById('w_alw').checked ? 1 : 0 ;
	
		$.ajax({
			type: "post",
			url: "<?=base_url()?>"+'BusinessLvlCtrl/change_withdraw_status',
			data: {allow :allow},
			dataType: "json",
			success: function (response) {
				if(response.success == 1){
					$('#rs').html(`<span class="badge badge-pill badge-success">Success</span>`);
					setTimeout(() => {
						$('#rs').html("");
					}, 2000);
				}
				else{
					$('#rs').html(`<span class="badge badge-pill badge-danger">Error</span>`);
					setTimeout(() => {
						$('#rs').html("");
					}, 2000);
				}
			}
		});
	
}

</script>
