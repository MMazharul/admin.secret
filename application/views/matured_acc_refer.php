<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-account-convert"></i>
		</span>
		Withdrawable Members (Refer + Others 500+) 
	</h3>
	
</div>

<div class="row">
	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">Matured Accounts &nbsp &nbsp ( Total: <span id='sum'><?php $total=0;echo $total; ?></span> )</h4>
				<div class="table-responsive">
					<table id="datatable" class="table dataTable display">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								<th>
									Refer Bonus<br>Earned
								</th>
								<th>
									Work<br>Earned
								</th>
								<th>
									Refer Work<br>Earned
								</th>
								<th>
									Other<br>Earned
								</th>
								<th>
									Total
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($matureds as $member):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$member->user_id?>" target="_blank"> <?= $member->name ?></a></td>
								<td class="total"><?= $member->refer_earn_amount ?></td>
								<td ><?= number_format($member->work_earn_amount ,2)?></td>
								<td><?= number_format($member->refer_work_earn_amount,2) ?></td>
								<td><?= number_format($member->other_earn_amount,2) ?></td>
								<td><?php echo number_format($member->total,2);$total+=$member->total; ?></td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
let tot=0;
$('.total').each(function(){
	tot+= parseFloat($(this).html().replace(/,/gi, ''));
});
$('#sum').html(tot);
</script>