<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-account-convert"></i>
		</span>
		Fund Request
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row">
	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">All Requestes</h4>
				<div class="table-responsive">
					<table id="datatable" class="table dataTable display">
						<thead>
						<tr>
							<th>
								Serial
							</th>
							<th>
								Package Amount
							</th>
							<th>
								Sender Number
							</th>
							<th>
								Transaction ID
							</th>
							<th>
								Bank Account
							</th>
							<th>
								Bank Slip
							</th>
							<th>
								Payment Method
							</th>
							<th>
								Requested At
							</th>
							<th>
								Actions
							</th>
						</tr>
						</thead>
						<tbody>
						<?php $serial=1; foreach($fund_reqsts as $fund):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><?= $fund->package_amount ?></td>
								<td><?= $fund->sender_number==null ? 'null' : $fund->sender_number?></td>
								<td><?= $fund->transaction_id==null ? 'null' : $fund->transaction_id ?></td>
								<td><?= $fund->bank_account==null ? 'null' : $fund->bank_account ?></td>
								<td><a href="http://work.bizbazar.test/uploads/payment_slip/<?=$fund->bank_slip?>" target="_blank">
									<?= $fund->bank_slip==null ? 'null' : '<img src="http://work.bizbazar.test/uploads/payment_slip/'.$fund->bank_slip.'" style="width:150px;height:93px;border-radius:0px"' ?>
								</td>
								<td><?= $fund->payment_method ?></td>
								<td><?= nice_date($fund->created_at, 'd-M-y') ?></td>
								<td>
									<button onclick="accpt_fund(this);" data-user_id="<?=$fund->user_id?>" data-package_amount="<?=$fund->package_amount+$fund->total_commission?>" data-package_id="<?=$fund->package_id?>" data-id="<?=$fund->id?>" class='btn btn-sm btn-rounded btn-gradient-success'><i class="mdi mdi-check"></i> <i class="mdi mdi-sync mdi-spin a_ldr"></i> </button>
									&nbsp &nbsp<button onclick=" reject_fund(this)" data-id="<?=$fund->id?>" class='btn btn-sm btn-rounded btn-gradient-danger'><i class="mdi mdi-close"></i><i class="mdi mdi-sync mdi-spin r_ldr"></i></button>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>fore
				</div>
			</div>
		</div>
	</div>
</div>

<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-history"></i>
		</span>
		Fund Request History
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row">
	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">All Requestes History</h4>
				<div class="table-responsive">
					<table id="datatable" class="table dataTable display">
						<thead>
						<tr>
							<th>
								Serial
							</th>
							<th>
								Package Amount
							</th>
							<th>
								Sender Number
							</th>
							<th>
								Transaction ID
							</th>
							<th>
								Bank Account
							</th>
							<th>
								Bank Slip
							</th>
							<th>
								Payment Method
							</th>
							<th>
								Requested At
							</th>
							<th>
								Checked At
							</th>
							<th>
								Status
							</th>
						</tr>
						</thead>
						<tbody>
						<?php $serial=1; foreach($fund_reqsts_his as $fund):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><?= $fund->package_amount ?></td>
								<td><?= $fund->sender_number==null ? 'null' : $fund->sender_number?></td>
								<td><?= $fund->transaction_id==null ? 'null' : $fund->transaction_id ?></td>
								<td><?= $fund->bank_account==null ? 'null' : $fund->bank_account ?></td>
								<td><a href="http://work.bizbazar.test/uploads/payment_slip/<?=$fund->bank_slip?>" target="_blank">
										<?= $fund->bank_slip==null ? 'null' : '<img src="http://work.bizbazar.test/uploads/payment_slip/'.$fund->bank_slip.'" style="width:150px;height:93px;border-radius:0px"' ?>
								</td>
								<td><?= $fund->payment_method ?></td>
								<td><?= nice_date($fund->created_at, 'd-M-y') ?></td>
								<td><?= nice_date($fund->updated_at, 'd-M-y') ?></td>
								<td>
									<span class="badge <?php if($fund->status == 2){echo 'badge-danger';}elseif($fund->status == 1){echo 'badge-success';}else{echo 'badge-danger';}?>"><?=$fund->status==1 ? 'Accepted':'Rejected'?></span>
								</td>
							</tr>
						<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>



<script>
	$(document).ready(function () {
		$('#datatable').DataTable();
		$(".a_ldr,.r_ldr").hide();
	});

	function accpt_fund(e) {

		let user_id = $(e).data('user_id');
		let package_id = $(e).data('package_id');
		let id = $(e).data('id');
		let package_amount = $(e).data('package_amount');
		let data = {
			status:1,
			user_id: user_id,
			package_amount: package_amount,
			package_id: package_id,
			id: id
		};

		if (confirm('Are You sure to Accept ?')) {
			$(".a_ldr").show();
			$.ajax({
				type: "post",
				url: "<?=base_url('FlexiloadCtrl/fund_accepted')?>",
				data: data,
				dataType: 'json',
				success: function (response) {
					console.log(response)
					if(response.success===true){

						location.reload();
					}
					else if(response.success===false){
						alert("Sometimes Wrong,Please try again !!");

					}
				},
				error:function () {
					location.reload();
				}
			});
		}

	}

	function reject_fund(e) {
		let id = $(e).data('id');
		let data = {
			id: id
		};
		if (confirm('Are You sure to Decline ?')) {
			$(".a_ldr").show();
			$.ajax({
				type: "post",
				url: "<?=base_url('FlexiloadCtrl/fund_rejected')?>",
				data: data,
				dataType: 'json',
				success: function (response) {
					if(response.success===true){

						location.reload();
					}
					else {
						location.reload();


					}
				},
				error:function () {
					alert("Error");
				}
			});
		}

	}
</script>
