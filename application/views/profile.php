<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-home"></i>
		</span>
		User Profile
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

 
	<div class="row">
		<div class="col-12 m-0 p-0">
			<img class="profile_cover shadow" src="<?=main_url('app-assets/images/covers/covers.jpg')?>" alt="">
		</div>

		<div class="col-12 d-flex justify-content-center">
			<input type="file" class="d-none" id="pic_input" onchange="document.getElementById('prfl_photo').src = window.URL.createObjectURL(this.files[0])">
			<div id="profile_pic_edit"><button class="btn btn-warning shadow btn-rounded btn-sm">Photo <i class="mdi mdi-image"></i> </button></div>
			<img id="prfl_photo" class="profile_img shadow" src="<?=main_url('app-assets/images/members/'.$user_data->picture)?>" alt="">

			
			<div id="profile_edit"><button onclick="edit_profile()" class="btn btn-outline-info shadow btn-rounded btn-sm">Edit <i class="mdi mdi-pencil"></i> </button></div>

			<div id="edit_close"><button onclick="edit_close()" class="btn btn-outline-danger shadow btn-rounded btn-sm"><i class="mdi mdi-close"></i> </button></div>
			<div id="save"><button onclick="save_prfile(this)" data-id="<?=$user_data->id ?>" class="btn btn-success shadow btn-rounded btn-sm">Save <i class="mdi mdi-content-save"></i><i class="mdi mdi-spin mdi-reload loading"></i> </button></div>
			<div id="save"></div>
		</div>

		<div class="col-12 d-flex justify-content-center mt-3">
			<h3 id="name" ><?=$user_data->name?></h3>
		</div>
		
		<div class="col-12 d-flex justify-content-center"><img class="profile_level_icon" src="<?=main_url('app-assets/images/level/'.$user_data->reputation_milestone_icon)?>"
			 alt=""></div>
		<div class="col-12 d-flex justify-content-center">
			<h4><?=$user_data->reputation_milestone_name?></h4>
		</div>
		<div class="col-12 d-flex justify-content-center">
			<small class="badge badge-pill badge-gradient-primary"><?=$user_data->total_refered?> Refer</small>
		</div>



	</div> 

	<div class="row justify-content-center">

			<div class="col-md-3 m-2">
				<div class="bg-white border10 p-2 shadow">
					<div class="m-2"><i class="mdi mdi-rotate-3d"></i><b> Status : </b>
					<?php if($user_data->active_status == 0){
						echo '<span class="badge badge-pill badge-gradient-danger mt-1">Inactive</span>';
					 } elseif ($user_data->active_status == 2) {
						echo '<span class="badge badge-pill badge-gradient-warning mt-1">Pending</span>';
					} elseif ($user_data->active_status == 3) {
						echo '<span class="badge badge-pill badge-gradient-danger mt-1">Blocked</span>';
					} else{ echo '<span class="badge badge-pill badge-gradient-success mt-1">Active</span>';
					}?>
					</div>
					<div class="m-2"><i class="mdi mdi-key"></i><b> Activation Pin : </b><span class="badge badge-pill bg-light"><?=$user_data->activation_pin?></span></div>
					<div class="text-center"><button onclick="resend_cr(this)" id="<?=$user_data->id?>" class="btn btn-success btn-rounded btn-sm shadow-sm ">Resend Mobile &<br>Activation PIN <i class="mdi mdi-sync mdi-spin loading"></i> </button></div>
					<?php if($user_data->active_status == 1 && !isset($_GET['user_id'])):?>
					<div class="m-2"><i class="mdi mdi-share"></i><b> Refer ID : </b><span class="badge badge-pill bg-warning"><?=$user_data->own_refer_id?></span></div>
					<div class="m-2"><i class="mdi mdi-share-variant"></i><b> Refer Link : </b> 
					<br>
					<div id="refer_link" class="bg-light p-2 border10"><small id="refer_link_copy" class="text-gray float-right m-1">copy</small><a href="http://work.Biz-Bazar.com?refer_id=<?=$user_data->own_refer_id?>" target="_blank"><span id="r_link">http://work.biz-bazar.com?refer_id=<?=$user_data->own_refer_id?></span></a></div>
					</div>
					<?php endif ?>
				
				</div>

				<div class="card mt-3 shadow border10">
					<a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
						<div class="card-header bg-white border10"><i class="mdi mdi-lock-outline"></i>Reset User Password<i class="mdi mdi-chevron-down"></i>
						</div>
					</a>
					<div id="collapseTwo" class="collapse" data-parent="#accordion">
						<div class="card-body p-2">

							<div class="form-group text-center">
								<label for="pass"><b>Reset Password</b></label>
								<input type="password" class="form-control" id="pass" placeholder="Password">
							</div>

							<div class="form-group text-center">
								<label for="c_pass"><b>Confirm Password</b></label>
								<input type="password" class="form-control" id="c_pass" placeholder="Confirm Password">
							</div>

							<div class="form-check form-check-info text-center">
								<label class="form-check-label" for="usr">
									<input type="checkbox" name="pay_method" value="1" class="form-check-input" id="usr" >Send new password to user Mobile
								</label>
							</div>

							<div class="form-group text-center">
								<button onclick="change_pass(this)" data-id="<?=$user_data->id?>" class="btn btn-sm btn-info btn-rounded">Save <i class="mdi mdi-sync mdi-spin loading"></i></button>
							</div>
							<div class="text-center m-0 p-0" id="response"></div>
						</div>
					</div>
				</div>

			</div>

			<div class="col-md-4 m-2">
				<div class="bg-white border10 p-2 shadow">

					<div class="m-2"><i class="mdi mdi-cellphone-iphone"></i><b> Mobile : </b><span id="mob">0<?=$user_data->mobile_num?></span></div> 

					<div class="m-2"><i class="mdi mdi-gender-male-female"></i><b> Gender : </b>
						<select id="gender" class="select" disabled >
							<option <?=$user_data->gender == 1 ? "selected" : null ?> value="1" id="">Male</option>
							<option <?=$user_data->gender == 0 ? "selected" : null ?> value="0" id="">Female</option>
						</select>
					</div>

					<div class="m-2"><i class="mdi mdi-email-outline"></i><b> Email : </b><span id="email"><?=$user_data->email?></span></div>
					
					<div class="m-2"><i class="mdi mdi-map-marker-circle"></i><b> Adress : </b><span id="adress"><?=$user_data->adress?></span></div>

					<div class="m-2"><i class="mdi mdi-credit-card"></i><b> National ID : </b><span id="nid"><?=$user_data->national_id?></span></div>

					<div class="m-2"><i class="mdi mdi-calendar-today"></i><b> Joined In : </b><?= nice_date($user_data->join_date,"d-M-Y")?></div>
					<div class="m-2"><i class="mdi mdi-calendar-today"></i><b> Actiavated In : </b><?= nice_date($user_data->acc_active_date,"d-M-Y")?></div>
				</div>
			</div>

			<div class="col-md-4 m-2">
				<div class="bg-white border10 p-2 shadow">
					<div class="m-2"><i class="mdi mdi-coin"></i><b> Balance <i class="mdi mdi-chevron-down"></i> </b></div>
					
					<div class="m-2"><i class="mdi mdi-worker"></i><b> Job Earned : </b><?=number_format($user_data->work_earn_amount,2)?></div> 

					<div class="m-2"><i class="mdi mdi-wifi"></i><b> Refer Commission Earned : </b><?=number_format($user_data->refer_work_earn_amount,2)?></div>

					<div class="m-2"><i class="mdi  mdi-lightbulb-outline"></i><b> Refer Bonus Earned : </b><?=$user_data->refer_earn_amount?></div>

					<div class="m-2"><i class="mdi mdi-cash-100"></i><b> Other Earned : </b><?=number_format($user_data->other_earn_amount,2)?></div>

					<div class="m-2"><i class="mdi mdi-cash-multiple"></i><b> Total Balance : </b><?=number_format($user_data->total_bal,2) ?></div>
					<a href="https://secret.admin.co/WithdrawReqCtrl/withdraw_histry?user_id=<?=$_GET['user_id']?>" class="btn btn-inverse-success btn-rounded">See Withdraw History</a>
				</div>
			</div>
	</div>


	<div class="row">
	<div class="card col-md-12 border20 shadow">
		<h4 class="mt-3">Refered Members</h4>
		<div class="table-responsive">
			<table class="table table-hover datatable">
				<thead>
					<tr>
						<th>
							#
						</th>
						<th>
							Name
						</th>
						<th>
							Status
						</th>
						<th>
							View
						</th>
					</tr>
				</thead>
				<tbody>
					<?php $serial=1; foreach($refered_mem as $mem):?>
					<tr>
						<td>
							<?= $serial++ ?>
						</td>
						<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$mem->id?>" target="_blank"><img src="<?=main_url().'/app-assets/images/members/'.$mem->picture?>"
								 alt="">
								<?= $mem->name ?></a></td>
						<td>
						<span class="badge badge-pill <?php if($mem->status=='Active'){echo 'badge-success';}elseif($mem->status=='Pending'){echo 'badge-warning';}else{echo 'badge-danger';}?>"><?=$mem->status?></span>
						</td>
						<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$mem->id?>" target="_blank" class="btn btn-sm btn-inverse-info btn-rounded"><i class="mdi mdi-eye-outline"></i></a></td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div> 





<script>
	function edit_profile() { 
		$("#name,#email,#adress,#nid").attr("contentEditable",true).addClass("editable");
		$("#gender").attr("disabled",false);
		$("#profile_pic_edit,#save").css("display","block");
		$("#profile_edit").css("display","none");
		$("#edit_close").css("display","block");
	}

	 function edit_close() {
		$("#name,#mob,#email,#adress,#nid").attr("contentEditable",false).removeClass("editable");
		$("#gender").attr("disabled",true);
		$("#profile_pic_edit,#save").css("display","none");
		$("#profile_edit").css("display","block");
		$("#edit_close").css("display","none");
	 }

	 $('#profile_pic_edit').click(function(){ $('#pic_input').trigger('click'); });
	 

	 $("#pic_input").on("change", function() { 
		var file = this.files[0];
		console.log(file.size);

	 })
	 function save_prfile(e) {
		let formData = new FormData();
		let img = document.getElementById("pic_input").files[0];

		
		 //let photo = a;
		 let user_id = $(e).data('id');
		 let name = $("#name").html();
		 let mob = $("#mob").html();
		 let gender = $("#gender").val();
		 let email = $("#email").html();
		 let adress = $("#adress").html();
		 let nid = $("#nid").html();
		formData.append("photo",img);
		formData.append("user_id",user_id);
		formData.append("name",name);
		formData.append("mob",mob);
		formData.append("gender",gender);
		formData.append("email",email);
		formData.append("adress",adress);
		formData.append("nid",nid);
		
		 $.ajax({
			 type: "post",
			 url: "https://"+window.location.hostname+"/ProfileCtrl/profile_edit",
			 data: formData,
			 processData: false,
                contentType: false,
			 success: function (response) {
				$("#name,#mob,#email,#adress,#nid").attr("contentEditable",false).removeClass("editable");
		$("#gender").attr("disabled",true);
		$("#profile_pic_edit,#save").css("display","none");
		$("#profile_edit").css("display","block");
		$("#edit_close").css("display","none");
			 }
		 });
		
	 }

	$("#refer_link_copy").on("click",() => {
		var range = document.getSelection().getRangeAt(0);
            range.selectNode(document.getElementById("r_link"));
            window.getSelection().addRange(range);
            document.execCommand("copy");
			$("#refer_link_copy").html("copied");

			setTimeout(() => {
				$("#refer_link_copy").html("copy");
			}, 2000);
	})


	function change_pass(e) {
		let pass = $("#pass").val();
		let c_pass = $('#c_pass').val();
		let user_mob = document.getElementById('usr').checked ? 1 : 2;
		let id = $(e).data('id');
		console.log(user_mob);

		let data= {
			pass:pass,
			c_pass:c_pass,
			id:id,
			user_mob:user_mob
		}

		if(!pass== "" && !c_pass== ""){

			if(pass == c_pass){
				$.ajax({
					type: "post",
					url: location.protocol+"//"+window.location.hostname+"/ProfileCtrl/change_pass",
					data: data,
					dataType: "json",
					success: function (response) {
						if(response.hasOwnProperty('success')){
							$("#pass").val("");
							$('#c_pass').val("");
							setTimeout(() => {
								$('.collapse').collapse('hide');
							}, 1000);
							
						}
						$("#response").html(response.response);
							setTimeout(() => {
								$("#response").html('')	;
							}, 2000);
					}
				});
			}
			else{
				$("#response").html('<span class="p-2 border10 bg-danger">Password Not Matched</span>');
				setTimeout(() => {
					$("#response").html('')	;
				}, 2000);
			}
		}
	  }



	
	
	
	
	
	function resend_cr(e) {
		if(confirm('Resend Users data in his mobile ?')){
		let id = e.id;
		$.ajax({
			type: "post",
			url: "<?= base_url('ProfileCtrl/resend_credential') ?>",
			data: {id:id},
			dataType: "json",
			success: function (response) {
				if(response.success == 1){
					document.getElementById(e.id).innerHTML += "<i class='mdi mdi-check'></i>";
					$(".loading").hide();
				}
			}
		});
		}
	}


</script>