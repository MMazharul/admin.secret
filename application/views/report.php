 <!-- include -->
 
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/daterangepicker.min.css"/>


<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-details"></i>
		</span>
		Reports
	</h3>
</div>

<div class="row justify-content-center">
	<div class="col-12 border10 shadow bg-white p-3">
		<div class="row justify-content-center">
			<div class="col-md-5 col-12 align-self-center p-2">
				<div id="fromDate" class="border10 " style="background: #fff;margin:auto; cursor: pointer; padding: 5px 10px; border: 1px solid #8bff; width: 300px">
					From- <i class="mdi mdi-calendar"></i>&nbsp;
					<span id="from"></span> <i class="mdi mdi-chevron-down"></i> <i class="mdi mdi-sync mdi-spin dateloading"></i>
				</div>
			</div>

			<div class="col-md-5 col-12 align-self-center p-2">
				<div id="toDate" class="border10 " style="background: #fff;margin:auto; cursor: pointer; padding: 5px 10px; border: 1px solid #8bff; width: 300px">
					To- <i class="mdi mdi-calendar"></i>&nbsp;
					<span id="to"></span> <i class="mdi mdi-chevron-down"></i> <i class="mdi mdi-sync mdi-spin dateloading"></i>
				</div>
			</div>
			<div class="col-2">
				<button onclick="search()" class="btn btn-outline-info btn-icon"><i class="mdi mdi-file-find mdi-36px"></i></button>
			</div>
		</div>
	</div>

	<?php if(isset($_GET['from']) && isset($_GET['to'])):  
				$intrvl = date_diff(date_create($_GET['from']),date_create($_GET['to']));
                $diff = $intrvl->format("%y Year %m Month %d Day %h Hours %i Minute %s Seconds");?>
    <div class="col-md-10 col-12 shadow bg-danger border20 p-3 m-3"><h4 class="text-center">From:<u><?=$_GET['from']?></u> <br> To: <u><?=$_GET['to']?></u></h4>
	<p class="text-center">Date Differance:<?=$diff?></p>
	</div>
    <?php endif ?>

	<div class="col-md-3 col-12 shadow bg-gradient-info border20 p-3 m-2"><h4 class="text-center">Total Refer Bonus Earned: <u>৳<?=$all_total->total_refer_earned?></u></h4></div>
    <div class="col-md-3 col-12 shadow bg-gradient-primary border20 p-3 m-2"><h4 class="text-center">Total Refer Commission Earned: <u>৳<?=number_format($all_total->total_refer_work_earned,2)?></u></h4></div>
    <div class="col-md-3 col-12 shadow bg-gradient-danger border20 p-3 m-2"><h4 class="text-center">Total Work Earned: <u>৳<?=number_format($all_total->total_work_earned,2)?></u></h4></div>
    <div class="col-md-3 col-12 shadow bg-gradient-success border20 p-3 m-2"><h4 class="text-center">Total Other Earned: <u>৳<?=number_format($all_total->total_other_earned,2)?></u></h4></div>
    <div class="col-md-3 col-12 shadow bg-gradient-light border20 p-3 m-2"><h4 class="text-center">Total Withdrawn: <u>৳<?=$all_total->total_withdrawn?></u></h4></div>
    <div class="col-md-6 col-12 shadow bg-gradient-dark text-white border20 p-3 m-2"><h4 class="text-center">Total All Earned: <u>৳<?=number_format($all_total->total_earned,2)?></u></h4></div>
    <div class="col-md-6 col-12 shadow bg-gradient-warning border20 p-3 m-2"><h4 class="text-center">Total Join Income: <u>৳<?=$income->total_income?></u></h4></div>


    <div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">All Members Earn <?php if(isset($_GET['from']) && isset($_GET['to'])):?> ( From:<u><?=$_GET['from']?></u> To: <u><?=$_GET['to']?></u> ) 
				<p>Date Differance:<?=$diff?></p> <?php endif ?>
				</h4>
						
				<div class="table-responsive">
					<table id="datatable" class="table display">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								<th>
									Total Refer<br>Bonus Earned
								</th>
								<th>
									Total Refer<br>Commission<br>Earned
								</th>
								<th>
									Total Work<br>Earned
								</th>
                                <th>
                                    Total Other<br>Earned
                                </th>
								<th>
                                    Total All<br>Earned
                                </th>
                                <th>
                                    Total<br>Withdrawn
                                </th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($all_members_total as $total):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$total->id?>" target="_blank"><img src="<?=main_url().'/app-assets/images/members/'.$total->picture?>" alt="">  <?= $total->name ?></a></td>
                                <td><a href="<?=base_url('ReportCtrl/report_details?refer_earned_id=').$total->id?><?php if(isset($_GET['from']) && isset($_GET['to'])){echo ('&from='.$_GET['from'].'&to='.$_GET['to']);}?>" target="_blank"><?=$total->total_refer_earned?> <i class="mdi mdi-eye-outline"></i></a></td>
                                <td><a href="<?=base_url('ReportCtrl/report_details?refer_work_earned_id=').$total->id?><?php if(isset($_GET['from']) && isset($_GET['to'])){echo ('&from='.$_GET['from'].'&to='.$_GET['to']);}?>" target="_blank"><?= number_format($total->total_refer_work_earned,2)?> <i class="mdi mdi-eye-outline"></i></a></td>
                                <td><a href="<?=base_url('ReportCtrl/report_details?work_earned_id=').$total->id?><?php if(isset($_GET['from']) && isset($_GET['to'])){echo ('&from='.$_GET['from'].'&to='.$_GET['to']);}?>" target="_blank"><?= number_format($total->total_work_earned,2)?> <i class="mdi mdi-eye-outline"></i></a></td>
                                <td><a href="<?=base_url('ReportCtrl/report_details?other_earned_id=').$total->id?><?php if(isset($_GET['from']) && isset($_GET['to'])){echo ('&from='.$_GET['from'].'&to='.$_GET['to']);}?>" target="_blank"><?= number_format($total->total_other_earned,2)?> <i class="mdi mdi-eye-outline"></i></a></td>
                                <td><?= number_format($total->total_refer_earned+$total->total_refer_work_earned+$total->total_work_earned+$total->total_other_earned,2)?></td>
                                <td><a href="<?=base_url('ReportCtrl/report_details?withdrawn_id=').$total->id?><?php if(isset($_GET['from']) && isset($_GET['to'])){echo ('&from='.$_GET['from'].'&to='.$_GET['to']);}?>" target="_blank"><?=$total->total_withdrawn?> <i class="mdi mdi-eye-outline"></i></a></td>
							</tr>
							<?php endforeach?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(window).on('load', function () {
		//$.getScript("<?= base_url() ?>app-assets/js/daterangepicker.min.js");

		$(function () {

			var start = moment().startOf('days');
			var end = moment();

			function cb(start, end) {
				$('#fromDate span').html(start.format('DD-MMM-YY h:m:ss a'));
			}

			$('#fromDate').daterangepicker({
				timePicker: true,
				singleDatePicker: true,
				showDropdowns: true,
				startDate: start,

				ranges: {
					'Today Now': [moment(), moment()],
					'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					'This Month': [moment().startOf('month'), moment().endOf('month')],
					'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				}
			}, cb);

			cb(start, end);

		});

		$(function () {

			var start = moment().endOf('days');
			var end = moment();

			function cb(start, end) {
				$('#toDate span').html(start.format('DD-MMM-YY h:m:ss a'));
			}

			$('#toDate').daterangepicker({
				timePicker: true,
				singleDatePicker: true,
				showDropdowns: true,
				startDate: start,

			}, cb);

			cb(start, end);

		});
		
		$('.dateloading').hide();
	});

    function search(){
        window.location.href = '<?=base_url('ReportCtrl')?>'+ '?from='+$('#from').html() + '&to=' + $('#to').html();
    }
</script>