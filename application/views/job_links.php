<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-link"></i>
		</span>
		Job Links
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row">
	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">Place Links :</h4>
				<form method="post" action="<?=base_url()?>/JobLinkCtrl/save_link">
					<div class="form-group">
						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text"><i class="mdi mdi-link-variant text-info"></i></span>
							</div>
							<input name="link" type="text" class="form-control" placeholder="Place Link Here...">
							<div class="input-group-append">
								<button class="btn btn-success" type="submit">Save <i class="mdi mdi-play"></i> </button> 
							</div>
						</div>
					</div>
				</form>
				<br>
				<h4 class="card-title">Links</h4>
				<div class="table-responsive">
					<table id="datatable" class="table dataTable display">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Job Links
								</th>
								<th>
									Delete
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($job_link as $link):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><a href="<?= $link->link ?>" target="_blank"><?= $link->link ?></a></td>
								<td><a onclick="return confirm('Are You Sure ?')" href="<?=base_url("/JobLinkCtrl/del_link?id=".$link->id)?>" class="btn btn-sm btn-inverse-danger btn-rounded" ><i class="mdi mdi-delete-variant mdi-18px"></i></a></td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>



<script>
	$(document).ready(function () {
		$('#datatable').DataTable();
	});

</script>
