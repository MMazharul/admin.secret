
<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-home"></i>
		</span> 
		Dashboard
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row">
	<div class="col-md-4  stretch-card grid-margin">
		<div class="card border20 bg-gradient-primary card-img-holder text-white shadow">
			<div class="card-body">
				<img src="<?= base_url() ?>app-assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
				<h4 class="font-weight-normal mb-3">Active Members
					<i class="mdi mdi-account-check  mdi-48px float-right"></i>
				</h4>
				<h1><?=$actv_mem[0]->active?></h1>
				<a href="<?=base_url("/MemberCtrl/all_members")?>" class="text-white">See All...</a>
			</div>
		</div>
	</div>

	<div class="col-md-4  stretch-card grid-margin">
		<div class="card border20 bg-gradient-danger card-img-holder text-white shadow">
			<div class="card-body">
				<img src="<?= base_url() ?>app-assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
				<h4 class="font-weight-normal mb-3">Inactive Members
					<i class="mdi mdi-account-remove  mdi-48px float-right"></i>
				</h4>
				<h1><?=$inactv_mem[0]->inactive?></h1>
				<a href="<?=base_url("/MemberCtrl/all_members#inactive_member")?>" class="text-white">See All...</a>
			</div>
		</div>
	</div>

	<div class="col-md-4  stretch-card grid-margin">
		<div class="card border20 bg-gradient-warning card-img-holder text-black shadow">
			<div class="card-body">
				<img src="<?= base_url() ?>app-assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
				<h4 class="font-weight-normal mb-3">Pending Members
					<i class="mdi mdi-account-alert  mdi-48px float-right"></i>
				</h4>
				<h1><?=$pend_mem[0]->pending?></h1>
				<a href="<?=base_url("/MemberCtrl/all_members#pending_member")?>" class="text-white">See All...</a>
			</div>
		</div>
	</div>
</div>

<div class="row">

	<div class="col-md-6  stretch-card grid-margin">
		<div class="card border20 bg-gradient-success card-img-holder shadow">
			<div class="card-body">
				<img src="<?= base_url() ?>app-assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
				<h4 class="font-weight-normal mb-3"><b>Recently Members Added</b>
					<i class="mdi mdi-account-plus mdi-48px float-right"></i>
				</h4>
				<div class="mb-2">Today Member Added : <b><?=$today_mem_add[0]->total_mem?></b></div>
				<div class="mb-2">Last 7 Day Member Added : <b><?=$svn_mem_add[0]->total_mem?></b></div>
				<div class="mb-2">Last 30 Day Member Added : <b><?=$thirty_mem_add[0]->total_mem?></b></div>
				<a href="<?=base_url("/MemberCtrl/all_members")?>" class="">See All...</a>
			</div>
		</div>
	</div>
	<div class="col-md-6  stretch-card grid-margin">
		<div class="card border20 bg-gradient-info card-img-holder shadow">
			<div class="card-body">
				<img src="<?= base_url() ?>app-assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
				<h4 class="font-weight-normal mb-3"><b>Withdrawn</b>
					<i class="mdi mdi-cash-multiple mdi-48px float-right"></i>
				</h4>
				<div class="mb-2">Today Withdrawn : <b>৳ <?=$today_withdraw[0]->withdraw?></b></div>
				<div class="mb-2">Last 7 Day Withdrawn : <b>৳ <?=$svn_withdraw[0]->withdraw?></b></div>
				<div class="mb-2">Last 30 Day Withdrawn : <b>৳ <?=$thirty_withdraw[0]->withdraw?></b></div>
				<div class="mb-2">Total Withdrawn : <b>৳ <?=$total_withdraw[0]->withdraw?></b></div>
				<a href="<?=base_url("/WithdrawReqCtrl#histry")?>" class="">See All...</a>
			</div>
		</div>
	</div>

</div>

<div class="row">
	<div class="col-md-6  stretch-card grid-margin">
		<div class="card border20 bg-gradient-primary card-img-holder shadow">
			<div class="card-body">
				<img src="<?= base_url() ?>app-assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
				<h4 class="font-weight-normal mb-3"><b>Users Earned</b>
					<i class="mdi mdi-coin mdi-48px float-right"></i>
				</h4>
				<div class="mb-2">Refer Join Earned : <b><?=number_format($users_earned[0]->refer_earn,2)	?></b></div>
				<div class="mb-2">Work Earned : <b><?=number_format($users_earned[0]->work_earn,2)?></b></div>
				<div class="mb-2">Refer Work Commission Earned : <b><?=number_format($users_earned[0]->refer_work_earn,2)?></b></div>
				<div class="mb-2">Other Earned : <b><?=number_format($users_earned[0]->other_earn,2)?></b></div>
				<div class="mb-2">Total Earned : <b><?=number_format($users_earned[0]->total_earn,2)?></b></div>
			</div>
		</div>
	</div>
	<div class="col-md-6  stretch-card grid-margin">
		<div class="card border20 bg-gradient-secondary card-img-holder shadow">
			<div class="card-body">
				<img src="<?= base_url() ?>app-assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
				<h4 class="font-weight-normal mb-3"><b>Registration Income</b>
					<i class="mdi mdi-cash mdi-48px float-right"></i>
				</h4>
				<div class="mb-2">Today : <b>৳ <?=$reg_income_tdy->total?></b></div>
				<div class="mb-2">Seven Days : <b>৳ <?=$reg_income_svn->total?></b></div>
				<div class="mb-2">Thirty Days : <b>৳ <?=$reg_income_thirty->total?></b></div>
				<div class="mb-2">Total : <b>৳ <?=$reg_income_total->total?></b></div>
			</div>
		</div>
	</div>

</div>

<div class="row">
	<div class="card col-md-12 border20 shadow">
		<h4 class="mt-3">Top 5 Referers</h4>
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>
							#
						</th>
						<th>
							Name
						</th>
						<th>
							Total Refers
						</th>
					</tr>
				</thead>
				<tbody>
					<?php $serial=1; foreach($top_referers as $refer):?>
					<tr>
						<td>
							<?= $serial++ ?>
						</td>
						<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$refer->id?>" target="_blank"><img src="<?=main_url().'/app-assets/images/members/'.$refer->picture?>"
								 alt="">
								<?= $refer->name ?></a></td>
						<td>
							<?=$refer->total_refered?>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<div class="m-3"><a href="<?=base_url(" MemberCtrl/all_members")?>">See More...</a> </div>
		</div>
	</div>
</div> 