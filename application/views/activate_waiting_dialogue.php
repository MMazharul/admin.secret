<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="row">

	<div class="col-12 alert alert-warning justify-content-center text-center">

		<div class="row">

			<div class="col-12 col-md-8 col-sm-12">

			<p>Dear User,<br>Your Activation Request has been sent, Account will activate soon <br> Please Wait for Account Activation. <br>Thank You</p>

			</div>

			<div class="col-4 col-md-4 col-sm-4 align-self-center">

				<i class="mdi mdi-autorenew mdi-48px text-warning"></i>

			</div>

		</div>

		

	</div>

</div>