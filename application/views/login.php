<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V1</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/images/logo.gif" width='250px' alt="IMG">
				</div>

				<form action="<?=base_url()?>LoginCtrl/login_req" method="post" class="login100-form validate-form">
					<span class="login100-form-title">
						Admin Login
					</span>


					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="admin_num" placeholder="Number">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-phone" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button  type="submit" class="login100-form-btn">
							Login
						</button>
					</div>
					
					</form>

					<!-- <div class="text-center p-t-12">
						<span class="txt1">
							Forgot
						</span>
						<a class="txt2" href="#">
							Username / Password?
						</a>
					</div>

					<div class="text-center p-t-136">
						<a class="txt2" href="#">
							Create your Account
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div> -->
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/vendor/bootstrap/js/popper.js"></script>
	<script src="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--=============<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/==================================================================================-->
	<script src="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/vendor/select2/select2.min.js"></script>
<!--=============<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/==================================================================================-->
	<script src="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="<?= str_replace('index.php',"",base_url()) ?>app-assets/login_page_asset/js/main.js"></script>

</body>
</html>