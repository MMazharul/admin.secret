
<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-volume-medium"></i>
		</span>
		Notice
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row">
	<div class="col-12">
		<div class="bg-white border10 shadow p-3">
			<h4>Create Notice</h4>

			<form action="<?=base_url("/NoticeCtrl/save_notice")?>" method="post">
			<textarea name="content" id="editor"></textarea>
			<div class="text-center p-1"><button type="submit" class="btn btn-rounded btn-gradient-info m-2">Save</button></div>
			</form>
		</div>
	</div>
</div>

<br>
<br>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card shadow p-0">
		<div class="card border10">
			<div class="card-body p-3">
				<h4 class="card-title">All Notices</h4>
				<div class="table-responsive">
					<table class="table display datatable" id="" >
						<thead>
							<tr>
								<th>#</th>
								<th>
									Notices
								</th>
								<th>
									Status
								</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php $serial=1; foreach($notices as $notice):?>
						<tr>
						<td><?=$serial++?></td>
						<td><?=$notice->notice_content?></td>
						<td><span class="badge badge-pill badge-dark"><?=$notice->status?></span></td>
						<td><a onclick="return confirm('Are You Sure ?')" href="<?=base_url("/NoticeCtrl/del_notice?id=".$notice->id)?>" class="btn btn-rounded btn-inverse-danger btn-sm"><i class="mdi mdi-delete-forever"></i></a></td>
						</tr>
						<?php endforeach?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ), {
        toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
        heading: {
            options: [
                { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' }
            ]
        }
		
    } )
        .catch( error => {
            console.error( error );
        } );
</script>