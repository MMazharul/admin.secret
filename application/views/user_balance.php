<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-account-convert"></i>
		</span>
		Members Current Balances
	</h3>
	
</div>

<div class="row">
	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">All Members Current Balances</h4>
				<div class="table-responsive">
					<table id="table" class="table datatable display">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								<th>
									Refer Bonus<br>Earned Balance
								</th>
								<th>
									Refer Work<br>Earned Balance
								</th>
								<th>
									Work<br>Earned Balance
								</th>
								<th>
									Other<br>Earned Balance
								</th>
								<th>
									Total<br>Earned Balance
								</th>
								<th>
									Actions
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($users_bal as $member):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$member->user_id?>" target="_blank"><img src="<?=main_url().'/app-assets/images/members/'.$member->picture?>" alt="">  <?= $member->name ?></a></td>
								<td><?= $member->refer_earn_amount ?></td>
								<td><?= number_format($member->refer_work_earn_amount,2) ?></td>
								<td><?= number_format($member->work_earn_amount,2) ?></td>
								<td><?= $member->other_earn_amount ?></td>
								<td><?=number_format($member->refer_earn_amount+$member->refer_work_earn_amount+$member->work_earn_amount+ $member->other_earn_amount,2)?></td>
								<td>
									<span onclick="bon_data_set(this)" data-toggle="modal" data-target="#bonusModal" data-name="<?=$member->name?>" data-id="<?=$member->user_id?>" data-mob="<?=$member->mobile_num?>" ><button class="btn btn-inverse-danger btn-sm btn-rounded" data-toggle="tooltip" title="Minus Amount"><i class="mdi mdi-coin"></i><i class="mdi mdi-minus"></i></button></span>
								</td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-account-convert"></i>
		</span>
		Balance Deduct History
	</h3>
	
</div>

<div class="row">
	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">All Members Balance Deduct History</h4>
				<div class="table-responsive">
					<table id="" class="table datatable display">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								<th>
									Refer Bonus<br>Earned Balance
								</th>
								<th>
									Refer Work<br>Earned Balance
								</th>
								<th>
									Work<br>Earned Balance
								</th>
								<th>
									Other<br>Earned Balance
								</th>
								<th>Reason</th>
								<th>
									Date
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($deduct_his as $member):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$member->user_id?>" target="_blank"><img src="<?=main_url().'/app-assets/images/members/'.$member->picture?>" alt="">  <?= $member->name ?></a></td>
								<td><?= $member->refer_bonus ?></td>
								<td><?= number_format($member->refer_work,2) ?></td>
								<td><?= number_format($member->work,2) ?></td>
								<td><?= $member->other ?></td>
								<td><?= $member->reason ?></td>
								<td><?= nice_date($member->created_at,'d-M-y') ?></td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>







<!-- //bonus modal -->
<div class="modal fade" id="bonusModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"> <i class="mdi mdi-coin"></i>Minus Amount from <b id="b_name"></b> Balance</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

	 	<input id="b_id" type="hidden">
	 	<input id="b_mob" type="hidden">

		 
		<h6><i class="mdi mdi-cash"></i>Refer Bonus Amount</h6>
		<input id="refer_bon_amnt" type="number" value="0" class="form-control">
		<h6><i class="mdi mdi-cash"></i>Refer Work Amount</h6>
		<input id="refer_work_amnt" type="number" value="0" class="form-control">
		<h6><i class="mdi mdi-cash"></i>Work Amount</h6>
		<input id="work_amnt" type="number" value="0" class="form-control">
		<h6><i class="mdi mdi-cash"></i>Other Amount</h6>
		<input id="other_amnt" type="number" value="0" class="form-control">
		<h6><i class="mdi mdi-textbox"></i>Balance Deduct Reason </h6>
		<input id="reason" type="text" class="form-control" value="Rules Violation">

		<h6><i class="mdi mdi-message-outline"></i> Sms Text</h6>
		<textarea id="bs_txt" cols="30" rows="3" class="form-control" placeholder="Write Here..." >Dear Biz-Bazar User, We notice this account break our some rules and regulation, We deduct some balance from your account.Thank You.</textarea>

		<br>
		<div id="b_result" class="text-center"></div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button onclick="deduct_amount()" type="button" class="btn btn-info">Save <i class="mdi mdi-sync mdi-spin loading"></i> </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>









<script>
$(document).ready(function () {
	$('.datatable').DataTable();
});
$(document).ready(function () {
	$('[data-toggle="tooltip"]').tooltip();
});

let table = document.getElementById('table');
let row=2;

function bon_data_set(e) {
	$('#b_name').html($(e).data('name'));
	$('#b_id').val($(e).data('id'));
	$('#b_mob').val($(e).data('mob'));

	row = e.parentNode.parentNode.rowIndex;
	$('#refer_bon_amnt').attr('max', table.rows[row].cells[2].innerHTML);
	$('#refer_work_amnt').attr('max', table.rows[row].cells[3].innerHTML);
	$('#work_amnt').attr('max', table.rows[row].cells[4].innerHTML);
	$('#other_amnt').attr('max', table.rows[row].cells[5].innerHTML);

	//console.log(table.rows[row].cells[5].innerHTML);
}

function deduct_amount() {
	$('.loading').show();
	let id = $('#b_id').val();
	let mob = $('#b_mob').val();
	let reason = $('#reason').val();
	let s_txt = $('#bs_txt').val();
	let refer_bon_amnt = parseFloat($('#refer_bon_amnt').val());
	let refer_work_amnt = parseFloat($('#refer_work_amnt').val());
	let work_amnt = parseFloat($('#work_amnt').val());
	let other_amnt = parseFloat($('#other_amnt').val());

	let total = refer_bon_amnt+refer_work_amnt+work_amnt+other_amnt;

	if (refer_bon_amnt > parseInt($('#refer_bon_amnt').attr('max')) || refer_work_amnt > parseInt($('#refer_work_amnt').attr('max')) || work_amnt > parseInt($('#work_amnt').attr('max')) || other_amnt > parseInt($('#other_amnt').attr('max'))) {
		$('#b_result').html('<span class="badge badge-danger">User have not enough balance, Please insert correct value in correct field.</span>');
		setTimeout(() => {
			$('#b_result').html('');
		}, 3000);
	} else {
		let data = {
			user_id: id,
			mob: mob,
			reason:reason,
			s_txt: s_txt,
			refer_bon_amnt: refer_bon_amnt,
			refer_work_amnt: refer_work_amnt,
			work_amnt: work_amnt,
			other_amnt: other_amnt
		};
		console.log(data);
		if(confirm("Are You Really Want to Minus Total: "+total+" Tk from this user?")){

		 $.ajax({
			type: "post",
			url: "<?=base_url()?>BalanceCtrl/deduct_amount",
			data: data,
			dataType: "json",
			success: function (response) {
				if(response.success==1){
					$('.loading').hide();
					$('#b_result').html('<span class="alert alert-success">Successfully Sended.</span>');
					setTimeout(() => {
						$('#b_result').html('');
						$('#refer_bon_amnt').val(0)
						$('#refer_work_amnt').val(0)
						$('#work_amnt').val(0)
						$('#other_amnt').val(0)
						$('.modal').modal('hide');
						
						table.rows[row].cells[2].innerHTML = parseFloat(table.rows[row].cells[2].innerHTML) - parseFloat(response.refer_bon_amnt);
						table.rows[row].cells[3].innerHTML = parseFloat(table.rows[row].cells[3].innerHTML) - parseFloat(response.refer_work_amnt);
						table.rows[row].cells[4].innerHTML = parseFloat(table.rows[row].cells[4].innerHTML) - parseFloat(response.work_amnt);
						table.rows[row].cells[5].innerHTML = parseFloat(table.rows[row].cells[5].innerHTML) - parseFloat(response.other_amnt);
						table.rows[row].cells[6].innerHTML = parseFloat(table.rows[row].cells[2].innerHTML) +parseFloat(table.rows[row].cells[3].innerHTML) +parseFloat(table.rows[row].cells[4].innerHTML) +parseFloat(table.rows[row].cells[5].innerHTML) ;
						
					}, 2000);
				}
				else{
					$('#b_result').html('<span class="alert alert-danger">Error Occured !</span>');
					setTimeout(() => {
						$('#b_result').html('');
						$('.loading').hide();
					}, 2000);
				}
			}
		}); 
	}
	}
}
</script>
