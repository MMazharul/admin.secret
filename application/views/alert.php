
<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-volume-medium"></i>
		</span>
		Alert
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div class="row">
	<div class="col-12">
		<div class="bg-white border10 shadow p-3">
			<h4> <i class="mdi mdi-message"></i>Alert</h4>

			<textarea id="sms_txt" style="height:60px;width:100%;border-radius:10px;padding:5px" resizeable="true" placeholder="Write Here..."></textarea>
			
			<div class="row justify-content-center ">
				<div class="col-md-3 col-12 text-center mx-auto">
					<select id="type" class="form-control d-inline" style="width:150px;border-radius:50px">
						<option value="sms">Mobile Sms</option>
						<option value="notification">Notification</option>
					</select>

					<select id="target_mem" class="form-control d-inline" style="width:150px;border-radius:50px">
						<option value="all">All Members</option>
						<option value="active">Active Members</option>
						<option value="inactive">Inactive Members</option>
						<option value="pending">Pending Members</option>
					</select>
					<button onclick="send()" class="btn btn-gradient-info m-2">Send <i class="mdi mdi-send"></i></button>
					<div id="sending" style="display:none" class="text-center">
						It may need some minutes to complete, Please Wait..
						<br>
						<br>
							<div>
								<div class="progress " style="height:20px;">
									<div class="progress-bar progress-bar-striped progress-bar-animated" style="height:50px;width: 95%"></div>
								</div>
								<div>Sending...</div>
							</div>
							<i class="mdi mdi-radioactive mdi-spin mdi-36px text-info"></i>
						</div>
						<div id="success" style="display:none" class="alert alert-success">
							Successfully,Sended <i class="mdi mdi-check"></i>
						</div>
						<div id="error" style="display:none" class="alert alert-success">
							Error Occured ! <i class="mdi mdi-close"></i>
						</div>

				</div>
				
						
				
			</div>

		</div>
	</div>
</div>

<br>
<br>

<div class="row">
	<div class="col-md-12 grid-margin stretch-card shadow p-0">
		<div class="card border10">
			<div class="card-body p-3">
				<h4 class="card-title">All Sended SMS <i class="mdi mdi-message-outline"></i> </h4>
				<div class="table-responsive">
					<table class="table display datatable" id="datatable" >
						<thead>
							<tr>
								<th>#</th>
								<th>
									Text
								</th>
								<th>
									Target
								</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
						<?php $serial=1; foreach($all_sms as $sms):?>
						<tr>
						<td><?=$serial++?></td>
						<td><?=$sms->text?></td>
						<td><?=$sms->target?></td>
						<td><?=nice_date($sms->created_at,"d-M-y")?></td>
						</tr>
						<?php endforeach?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-12 grid-margin stretch-card shadow p-0">
		<div class="card border10">
			<div class="card-body p-3">
				<h4 class="card-title">All Sended Notifications <i class="mdi mdi-message-outline"></i> </h4>
				<div class="table-responsive">
					<table class="table display datatable" id="datatable" >
						<thead>
							<tr>
								<th>#</th>
								<th>
									Notification Text
								</th>
								<th>
									Target
								</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
						<?php $serial=1; foreach($all_notification as $notification):?>
						<tr>
						<td><?=$serial++?></td>
						<td><?=$notification->notification_txt?></td>
						<td><?php if($notification->target == 3){echo "All Member";}else if($notification->target == 1){echo "Active Members";}else if($notification->target == 0){echo "Inactive Members";}else{echo "Pending Members";}?></td>
						<td><?=nice_date($notification->created_at,"d-M-y")?></td>
						</tr>
						<?php endforeach?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	function send() {
		$("#sending").css("display","block");
		$("button").attr("disabled",true).css('pointer-events','none');

		let sms_txt = $("#sms_txt").val();
		let target = $("#target_mem").val();
		let type = $("#type").val();

		let data = {
			sms_txt : sms_txt,
			target : target,
			type:type
		};
		
		$.ajax({
			type: "post",
			url: "<?=base_url()?>/AlertCtrl/send_sms_to_members",
			data: data,
			dataType: "json",
			success: function (response) {
				if(response.process == "complete"){
					$("#sms_txt").val("");
					$("#sending").css("display","none");
					$("#success").css("display","block");
					$("button,a").attr("disabled",false).css('pointer-events','auto');
					setTimeout(() => {
						$("#success").css("display","none");
					}, 5000);
				}
				else{
					$("button,a").attr("disabled",false).css('pointer-events','auto');
					$("#error").css("display","block");
					setTimeout(() => {
						$("#error").css("display","none");
					}, 5000);
				}
			}
		});
	}
</script>