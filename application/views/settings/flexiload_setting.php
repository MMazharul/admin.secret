
<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-phone-voip"></i>
		</span>
		Flexiload Setting
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div >
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Api & Payment Setting</h4>

			<form class="forms-sample" method="post" action="<?=base_url('setting/flexiload_setting/'.$setting->id)?>">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="bkash_number">Bkash Number</label>
							<input type="number" class="form-control" value="<?=$setting->bkash_number?>"  name="bkash_number" id="bkash_number" placeholder="Package Amount">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="bkash_type">Bkash (Persional / Agent)</label>
							<input type="text" class="form-control" value="<?=$setting->bkash_type?>" name="bkash_type" id="bkash_type" placeholder="Validity Time">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="bank_name">Bank Name</label>
							<input type="text" class="form-control" value="<?=$setting->bank_name?>"   name="bank_name" id="bank_name" placeholder="Bank Name">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="branch_name">Branch Name</label>
							<input type="text" class="form-control" value="<?=$setting->branch_name?>" name="branch_name" id="branch_name" placeholder="Branch Name" >
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="commission">Account Number</label>
							<input type="text" class="form-control" value="<?=$setting->account_number?>"  name="account_number" id="account_number" placeholder="Bank Account Number">
						</div>
					</div>

				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="exampleInputName1">Flexiload Api Key</label>
							<input type="text" class="form-control" value="<?=$setting->api_key?>"  name="api_key" id="api_key" placeholder="api key">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="validity">Flexiload Pin Code</label>
							<input type="text" class="form-control" value="<?=$setting->pin_number?>" name="pin_number" id="pin_number" placeholder="pin number">
						</div>
					</div>
				</div>



				

				<button  class="btn btn-gradient-primary mr-2">Update</button>

			</form>
		</div>
	</div>
</div>

