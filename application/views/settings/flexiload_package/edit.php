<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="card">
	<div class="card-body">
		<h4 class="card-title">Flexiload Package Edit</h4>

		<form class="forms-sample" method="post" action="<?=base_url('setting/package_store/'.$package->id)?>">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="exampleInputName1">Package Amount</label>
						<input type="number" class="form-control" onblur="persentAmount()" name="package_amount" value="<?=$package->package_amount?>" id="package_amount" placeholder="Package Amount">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="validity">Validity Time</label>
						<input type="text" class="form-control" value="<?=$package->vadility?>" name="vadility" id="vadility" placeholder="Validity Time">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="commission">Commission (%)</label>
						<input type="number" class="form-control" value="<?=$package->commission?>" onblur="persentAmount()"  name="commission" id="commission" placeholder="Commission Persent">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="total_commission">Total Commission</label>
						<input type="number" class="form-control" name="total_commission" value="<?=$package->total_commission?>" id="total_commission" placeholder="Total Commission" readonly>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-sm-3 col-form-label">Status</label>
						<div class="col-sm-4">
							<div class="form-check">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="status" id="membershipRadios1" value="1" <?php if($package->status==1) echo 'checked' ?>>
									Active
									<i class="input-helper"></i></label>
							</div>
						</div>
						<div class="col-sm-5">
							<div class="form-check">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="status" id="membershipRadios2" value="0" <?php if($package->status==0) echo 'checked' ?>>
									Non-Active
									<i class="input-helper"></i></label>
							</div>
						</div>
					</div>
				</div>
			</div>

			<button onclick="persentAmount()" class="btn btn-gradient-primary mr-2">Update</button>
			<a href="<?=base_url('setting/package_list')?>" class="btn btn-light">Cancel</a>
		</form>
	</div>
</div>
<script type="text/javascript">
	function persentAmount() {
		let commissionPersent = document.getElementById("commission").value;
		let package_amount=document.getElementById("package_amount").value;
		let commission = package_amount*commissionPersent/100;
		document.getElementById("total_commission").value=commission;
		console.log(commission);
	}

</script>
