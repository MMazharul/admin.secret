
<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-phone-voip"></i>
		</span>
		Flexiload Package
	</h3>
	<nav aria-label="breadcrumb">
		<ul class="breadcrumb">
			<li class="breadcrumb-item active" aria-current="page">
				<span></span>Overview
				<i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
			</li>
		</ul>
	</nav>
</div>

<div >
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">Flexiload Package</h4>

			<form class="forms-sample" method="post" action="<?=base_url('setting/package_store/')?>">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="exampleInputName1">Package Amount</label>
							<input type="number" class="form-control" onblur="persentAmount()" name="package_amount" id="package_amount" placeholder="Package Amount">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="validity">Validity Time</label>
							<input type="text" class="form-control" name="vadility" id="vadility" placeholder="Validity Time">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="commission">Commission (%)</label>
							<input type="number" class="form-control" onblur="persentAmount()"  name="commission" id="commission" placeholder="Commission Persent">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="total_commission">Total Commission</label>
							<input type="number" class="form-control" name="total_commission" id="total_commission" placeholder="Total Commission" readonly>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-sm-3 col-form-label">Status</label>
							<div class="col-sm-4">
								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="status" id="membershipRadios1" value="1">
										Active
										<i class="input-helper"></i></label>
								</div>
							</div>
							<div class="col-sm-5">
								<div class="form-check">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="status" id="membershipRadios2" value="0">
										Non-Active
										<i class="input-helper"></i></label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<button onclick="persentAmount()" class="btn btn-gradient-primary mr-2">Submit</button>

			</form>
		</div>
	</div>
</div>

<br>
<br>



<div class="row">
	<div class="col-md-12 grid-margin stretch-card shadow p-0">
		<div class="card border10">
			<div class="card-body p-3">
				<h4 class="card-title">All Packages</h4>
				<div class="table-responsive">
					<table class="table display datatable" id="" >
						<thead>
						<tr>
							<th>#</th>
							<th>
								Package Amount
							</th>
							<th>
								Commission (%)
							</th>
							<th>
								Total Commission (Tk)
							</th>
							<th>
								Status
							</th>
							<th>Action</th>
						</tr>
						</thead>
						<tbody>
						<?php $serial=1; foreach($packages as $package):?>
							<tr>
								<td><?=$serial++?></td>
								<td><?=$package->package_amount?></td>
								<td><?=$package->commission	?></td>
								<td><?=$package->total_commission ?></td>
								<td><span class="badge badge-pill badge-dark"><?php if($package->status==1) echo 'Active'; else echo 'Non-Active'?></span></td>
								<td>
									<a onclick="return confirm('Are You Sure ?')" href="<?=base_url("/setting/package_delete?id=".$package->id)?>" class="btn btn-rounded btn-inverse-danger btn-sm"><i class="mdi mdi-delete-forever"></i></a>
									<a onclick="return confirm('Are You Sure ?')" href="<?=base_url("/setting/package_edit?id=".$package->id)?>" class="btn btn-rounded btn-inverse-danger btn-sm"><i class="mdi mdi-tooltip-edit"></i></a>
									<a>
										<label class="switch">
											<input  onchange="updateStatus(this)" data-id="<?=$package->id?>" data-status="<?=$package->status?>" type="checkbox" <?php if($package->status==1) echo 'checked'?>>
											<span class="slider round"></span>
										</label>
									</a>
								</td>
							</tr>
						<?php endforeach?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	const persentAmount=()=>
	{
		let commissionPersent = document.getElementById("commission").value;
		let package_amount=document.getElementById("package_amount").value;
		let commission = package_amount*commissionPersent/100;
		document.getElementById("total_commission").value=commission;
		console.log(commission);
	}
	const updateStatus=(e)=>
	{
		if(confirm('Are Sure to change Package status ?')){
			let id = $(e).data('id');
			let status = $(e).data('status');

			$.ajax({
				type: "post",
				url: "<?=base_url('setting/update_status')?>",
				data: {id:id,status:status},
				dataType: "json",
				success: function (response) {
					if(response.success==true)
					{
						location.reload();
					}
					else
					{
						alert("sometimes wrong");

					}
				}
			});
		}


	}


</script>
