  <!-- include -->

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/daterangepicker.min.css"/>


<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-details"></i>
		</span>
		Report Details
	</h3>
	
</div>

<div class="row">
	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title"><?=$title?></h4>
				 <?php if(isset($_GET['from'])&&isset($_GET['to'])){echo "<p>Date Differance :".$date_diff."</p>";}?>
						<div class="">
							<div id="fromDate" class="border10 d-none" style="background: #fff;margin-left:auto; cursor: pointer; padding: 5px 10px; border: 1px solid #8bff; width: 300px">
							From- <i class="mdi mdi-calendar"></i>&nbsp;
								<span id="from"></span> <i class="mdi mdi-chevron-down"></i> <i class="mdi mdi-sync mdi-spin dateloading"></i>
							</div>
						</div>
						<br>
						<div class="">
							<div id="toDate" class="border10 d-none" style="background: #fff;margin-left:auto; cursor: pointer; padding: 5px 10px; border: 1px solid #8bff; width: 300px">
							To- <i class="mdi mdi-calendar"></i>&nbsp;
								<span id="to"></span> <i class="mdi mdi-chevron-down"></i> <i class="mdi mdi-sync mdi-spin dateloading"></i>
							</div>
						</div>
						<br>
				<div class="table-responsive">
					<table id="datatable" class="table datatable display">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								<?php if(isset($from_user)){?>
								<th>
									From User	
								</th>
								<?php }?>
								<th>
									Amount
								</th>
								<th>
									Date
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; 
							
							foreach($datas as $data):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$data->id?>" target="_blank"><img src="<?=main_url().'/app-assets/images/members/'.$data->picture?>" alt="">  <?= $data->name ?></a></td>
						<?php if(isset($from_user)){?> <td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$data->f_id?>" target="_blank"><img src="<?=main_url().'/app-assets/images/members/'.$data->f_picture?>" alt="">  <?= $data->f_name ?></a></td> <?php } ?>
								<td><?= $data->amount ?></td>
								<td><?= nice_date($data->created_at,'d-M-y --  h:i:s a') ?></td>
								
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	$(window).on('load', function () {
		//$.getScript("<?= base_url() ?>app-assets/js/daterangepicker.min.js");

		$(function () {

			var start = moment().startOf('days');
			var end = moment();

			function cb(start, end) {
				$('#fromDate span').html(start.format('DD-MMM-YY -- h:m:ss a'));
			}

			$('#fromDate').daterangepicker({
				timePicker: true,
				singleDatePicker: true,
				showDropdowns: true,
				startDate: start,

				ranges: {
					'Today Now': [moment(), moment()],
					'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					'This Month': [moment().startOf('month'), moment().endOf('month')],
					'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				}
			}, cb);

			//cb(start, end);

		});

		$(function () {

			var start = moment().endOf('days');
			var end = moment();

			function cb(start, end) {
				$('#toDate span').html(start.format('DD-MMM-YY -- h:m:ss a'));
			}

			$('#toDate').daterangepicker({
				timePicker: true,
				singleDatePicker: true,
				showDropdowns: true,
				startDate: start,

			}, cb);

			//cb(start, end);

		});
		
		$('.dateloading').hide();
	});


	
</script>