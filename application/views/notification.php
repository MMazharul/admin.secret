
<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-info text-white mr-2">
			<i class="mdi mdi-bell-outline mdi mdi-24px"></i>
		</span>
		Notifications
	</h3>
</div>

<div class="row">
	<div class="col-12 border10">
	
		<?php foreach($notifications as $notify):?>
		<a href="<?=base_url().$notify->link.'?notify_id='.$notify->id?>" style="text-decoration: none;color: darkslategrey;">
			<div class="shadow-sm border10 mb-1 p-3 <?=$notify->viewed == 0 ? "alert-info" : "bg-white"?> ">
			<small class="float-right text-grey"><time datetime="<?=$notify->created_at?>"><?=time_ago($notify->created_at)?></time></small>
				<div class="d-flex">
					<div class="preview-thumbnail">
						<div class="notify-icon">
							<?=$notify->icon?>
						</div>
					</div>
					<div class="preview-item-content d-flex align-items-start flex-column justify-content-center">
						<div style="width:100%"><h6 class="preview-subject font-weight-normal mb-1"><?=$notify->title?>&nbsp &nbsp &nbsp &nbsp</h6></div>
					</div>
				</div>
				
				<div>
					<p class="mb-0 ml-3">
						<?=$notify->notification_txt?>
					</p>
				</div>
			</div>
		</a>
		
		<?php endforeach ?>
	
	</div>
</div>







