<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-share-variant"></i>
		</span>
		Refer Network
	</h3>
	
</div>

<div class="row">
	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">Connections</h4>
				<div class="table-responsive">
					<table id="datatable" class="table dataTable display">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								<th>
									Referer Name
								</th>
								<th>
									Referer Percentage
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($networks as $network):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$network->id?>" target="_blank"><img src="<?=main_url().'/app-assets/images/members/'.$network->picture?>" alt="">  <?= $network->name ?></a></td>
								<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$network->r_id?>" target="_blank"><img src="<?=main_url().'/app-assets/images/members/'.$network->r_picture?>" alt="">  <?= $network->r_name ?></a></td>
								<td><?= $network->referer_percentage ?></td>
								
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


