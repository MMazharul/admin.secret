<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-cash"></i>
        </span>
        Withdraw Request
    </h3>
    <nav aria-label="breadcrumb">
        <ul class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">
                <span></span>Overview
                <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
            </li>
        </ul>
    </nav>
</div>

<div class="row">
    <div class="col-12 grid-margin">
        <div class="card shadow border10">
            <div class="card-body">

                <h4 class="card-title">All Withdraw Request</h4>
                <h5>Total Requested Amount : <b>৳
                        <?= $w_total->total ?></b></h5>
                <div class="table-responsive">
                    <table id="datatable_print" class="table display">
                        <thead>
                            <tr>
                                <th>
                                    Serial
                                </th>
                                <th>
                                    Member
                                </th>
                                <th>
                                    Recieve Number
                                </th>
                                <th>
                                    Requested Amount
                                </th>
                                <th>
                                    User Total Balance
                                </th>
                                <th>
                                    Requested At
                                </th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $serial = 1;
                            foreach ($withdraw_req as $reqst) : ?>
                            <tr>
                                <td>
                                    <?= $serial++ ?>
                                </td>
                                <td><a href="https://secret.admin.co//ProfileCtrl?user_id=<?=$reqst->user_id?>"><?= $reqst->name ?></a>
                                </td>
                                <td>0<?= $reqst->receive_number ?>
                                </td>
                                <td class="alert-danger">
                                    <?= $reqst->amount - $reqst->bkash_fee ?> <sub>&nbsp
                                        <?= $reqst->amount ?> (without fee)</sub>
                                </td>
                                <td class="alert-warning">
                                    <?= number_format($reqst->user_total_bal, 2) ?>
                                </td>
                                <td>
                                    <?= nice_date($reqst->created_at, 'd-M-y') ?>
                                </td>
                                <td>
                                    <button onclick="set_modal_data(this)" data-usr_num="<?= $reqst->mobile_num ?>" data-user_id="<?= $reqst->user_id ?>" data-name="<?= $reqst->name ?>" data-rcv-num="<?= $reqst->receive_number ?>" data-req-amnt="<?= $reqst->amount ?>" data-id="<?= $reqst->id ?>" data-user_total="<?= $reqst->user_total_bal ?>" data-pm="<?= $reqst->pay_method ?>" data-date="<?= $reqst->created_at ?>" data-ref_ern_amnt="<?= $reqst->refer_earn_amount ?>" data-wrk_ern_amnt="<?= $reqst->work_earn_amount ?>" data-ref_wrk_ern_amnt="<?= $reqst->refer_work_earn_amount ?>" data-other_amnt="<?= $reqst->other_earn_amount ?>" data-toggle="modal" data-target="#modal" class='btn btn-sm btn-rounded btn-primary'>View
                                        <i class="mdi mdi-eye-outline"></i></button>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->session->flashdata('title') ?>
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border20">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Withdraw Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-12">
                        <div class="bg-light border10 p-2">
                            <h4>Name :<span id=_name></span></h4>
                            <hr>
                            <div>Job Earn :<span id="_wrk_ern" class="alert-info border10 p-1 d-inline"></span> BDT </div>
                            <br>
                            <div>Refer Commission : <span id="_ref_ern" class="alert-info border10 p-1"></span> BDT</div>
                            <br>
                            <div>Refer Bonus : <span id="_ref_bon" class="alert-info border10 p-1"></span> BDT</div>
                            <br>
                            <br>
                            <div>Other Earn : <span id="_othr_ern" class="alert-info border10 p-1"></span> BDT</div>
                            <br>
                            <div>Total Balance : <h4 id="_tot" class="badge-success d-inline border10 p-1 m-2"></h4> BDT</div>
                            <br>
                            <br>
                            <div>Requested amount : <h4 id="_req_amnt" class="d-inline border10 p-1 badge-warning"></h4> BDT
                            </div>
                            <br>
                            <div class="row">
                                <br>
                                <br>
                                <div class="col-7">Receive Number: <br><br>
                                    <h4 id="_rcv_num" class="d-inline border10 p-1 badge-dark"></h4>
                                </div>
                                <div class="col-5">
                                    Payment Method : <br><br>
                                    <h4 id="_pm" class="d-inline badge-gradient-primary p-1 border10 m-auto"></h4>
                                </div>
                            </div>
                            <br>
                            <div>Requested Date : <span id="_req_date" class="alert-success border10 p-1"></span></div>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button onclick="reject_req()" type="button" class="btn btn-danger">Reject</button>
                <button onclick="accept_req()" type="button" class="btn btn-primary">Send</button>
            </div>

            <div id="reject" class="align-self-center" style="display:none">
                <label for="reject_cuz">Reject Cause</label>
                <textarea id="reject_cuz" class="form-control" cols="50" rows="4"></textarea>
                <br>
                <label><input onchange="radioChange(this)" type="radio" name="cuz" value="limit">Limit Exceed</label>
                <label><input onchange="radioChange(this)" type="radio" name="cuz" value="in_job"> Insufficient Job</label>
                <label><input onchange="radioChange(this)" type="radio" name="cuz" value="in_refer"> Insufficient Refer</label>
                <p class="text-center text-primary">Are You Sure ?</p>
                <div class="text-center">
                    <button onclick="reject_reqst(this)" id="reject_link" class="btn btn-danger">Yes,Reject <i class="mdi mdi-sync mdi-spin loading"></i> </button>
                    <button data-dismiss="modal" class="btn btn-success">No</button>
                </div>
                <br>
                <br>
            </div>

            <form id="accept" style="display:none" action="<?= base_url() ?>/WithdrawReqCtrl/acpt_withdr_req" method="post">
                <div class="row p-3">
                    <div class="col-12 form-group">
                        <label for="snd_num">&nbsp Sender Number</label>
                        <input name="snd_num" type="number" min="1000000000" class="form-control" id="snd_num" placeholder="Sender Number" required>
                    </div>
                    <input id='user_id' type="hidden" name="user_id">
                    <input id='user_num' type="hidden" name="user_num">
                    <input id='id' type="hidden" name="id">
                    <div class="col-12 form-group">
                        <label for="rcv_num">&nbsp Receive Number</label>
                        <input name="rcv_num" type="text" class="form-control" id="rcv_num" placeholder="Receive Number" readonly required>
                    </div>
                    <div class="col-12 form-group">
                        <label for="tran_id">&nbsp Transection ID</label>
                        <input name="tran_id" type="text" class="form-control" id="tran_id" placeholder="Transection ID" required>
                    </div>
                    <div class="col-12 form-group">
                        <label for="j_amnt">&nbsp Job Earn Amount</label>
                        <input name="j_amnt" type="Number" class="form-control" id="j_amnt" placeholder="Job Earn Amount" required>
                    </div>
                    <div class="col-12 form-group">
                        <label for="ref_com_amnt">&nbsp Refer Commission Earn Amount</label>
                        <input name="ref_com_amnt" type="Number" class="form-control" id="ref_com_amnt" placeholder="Refer Commission Earn Amount" required>
                    </div>
                    <div class="col-12 form-group">
                        <label for="ref_bon_amnt">&nbsp Refer Bonus Earn Amount</label>
                        <input name="ref_bon_amnt" type="Number" class="form-control" id="ref_bon_amnt" placeholder=" Refer Bonus Earn Amount" required>
                    </div>
                    <div class="col-12 form-group">
                        <label for="otr_bon_amnt">&nbsp Other Bonus Earn Amount</label>
                        <input name="otr_bon_amnt" type="Number" class="form-control" id="otr_bon_amnt" placeholder="Other Bonus Earn Amount" required>
                    </div>

                    <div class="col-12 form-group">
                        <label for="tot_amnt">&nbsp Total Given Amount</label>
                        <input readonly name="tot_amnt" type="number" class="form-control" id="tot_amnt" placeholder="Total Amount" required>
                    </div>

                    <div class="col-4">
                        <button class="btn btn-gradient-dark btn-rounded btn-block" type="submit">Send</button>
                        <div id="loader" class=""><i class="mdi mdi-spin mdi-loading mdi-36px"></i></div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>


<a id="histry" href="#histry"></a>

<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-danger text-white mr-2">
            <i class="mdi mdi-cash-multiple"></i>
        </span>
        Withdraw History
    </h3>
</div>

<div class="row p-0">
    <div class="col-12 m-0 p-0">
        <div class="card shadow border10 m-0">
            <div class="card-body p-3">
                <h4 class="card-title">All History</h4>
                <div class="table-responsive">
                    <table id="datatable" class="table dataTable display">
                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Member
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Receive<br>Number
                                </th>
                                <th>
                                    Requested Amount
                                </th>
                                <th>
                                    Paid Amount
                                </th>
                                <th>
                                    Opration Date
                                </th>
                                <th>Details</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $serial = 1;
                            foreach ($withdraw_his as $his) : ?>
                            <tr>
                                <td>
                                    <?= $serial++ ?>
                                </td>
                                <td>
                                    <?= $his->name ?>
                                </td>
                                <td>
                                    <span class="badge <?php if ($his->status == " Paid") {
                                                            echo "badge-success";
                                                        } elseif ($his->status == "Pending") {
                                                            echo "badge-warning";
                                                        } else {
                                                            echo "badge-danger";
                                                        } ?>">
                                        <?= $his->status ?> </span>
                                </td>
                                <td>
                                    <?= "0" . $his->receive_number ?>
                                </td>
                                <td>
                                    <span class="badge badge-info">৳
                                        <?= $his->amount ?></span>
                                </td>
                                <td>
                                    <span class="badge badge-warning text-dark">৳
                                        <?= $his->paid_total_amount ?></span><br>
                                </td>
                                <td>
                                    <?= nice_date($his->updated_at, "d-M-Y") ?>
                                </td>
                                <td><button onclick="set_data_modal(this)" data-name="<?= $his->name ?>" data-status="<?= $his->status ?>" data-req-amount="<?= $his->amount ?>" data-paid-amount="<?= $his->paid_total_amount ?>" data-admin-snd-num="<?= $his->admin_snd_num ?>" data-tran-id="<?= $his->transection_id ?>" data-checked-at="<?= $his->updated_at ?>" data-requested-at="<?= $his->created_at ?>" data-rcv-num="<?= $his->receive_number ?>" data-pay-method="<?= $his->pay_method ?>" data-job-earn="<?= $his->paid_work_amnt ?>" data-refer-work-amnt="<?= $his->paid_refer_work_amnt ?>" data-refer-bon-amnt="<?= $his->paid_refer_bon_amnt ?>" data-other_amnt="<?= $his->paid_other_amnt ?>" data-toggle="modal" data-target="#withdraw_details_modal" class="btn btn-sm btn-rounded btn-gradient-dark">Details <i class="mdi mdi-details"></i></button>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade border10" id="withdraw_details_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Withdraw History Deatils</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-12">
                        <div class="mb-2"><b>Name :</b> <span id="name"></span></div>
                        <div class="mb-2"><b>Status :</b> <span id="status" class="badge badge-warning"></span></div>
                        <div class="mb-2"><b>Receive Number :</b> <span id="rcv-num" class="badge badge-pill badge-dark"></span></div>
                        <div class="mb-2"><b>Admin Send Number :</b> <span id="admin-snd-num" class="badge badge-pill badge-dark"></span></div>
                        <div class="mb-2"><b>Transection ID :</b> <span id="tran-id"></span></div>
                        <div class="mb-2"><b>Pay Method :</b> <span id="pay-method"></span></div>
                        <div class="mb-2"><b>Requested Amount :</b> <span id="req-amount"></span></div>
                        <div class="mb-2"><b>Paid Amount :</b> <span id="paid-amount"></span></div>
                        <div class="mb-2 ml-3"><b>Paid Job Amount :</b> <span id="job-earn"></span></div>
                        <div class="mb-2 ml-3"><b>Paid Refer Commission Amount :</b> <span id="refer-work-amnt"></span></div>
                        <div class="mb-2 ml-3"><b>Paid Refer Bonus Amount :</b> <span id="refer-bon-amnt"></span></div>
                        <div class="mb-2 ml-3"><b>Paid Other Bonus Amount :</b> <span id="other_amnt"></span></div>
                        <hr>
                        <div class="mb-2"><b>Requested At :</b> <span id="checked-at"></span></div>
                        <div class="mb-2"><b>Checked At :</b> <span id="requested-at"></span></div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<div id="result" class="alert alert-dark shadow text-center " style="position:absolute;right:40px;bottom:15px;display:none"></div>
<script>
    $(document).ready(function() {
        $('#datatable_print').DataTable({
            dom: 'Bfrtip',
            buttons: [{
                extend: 'print',
                className: 'btn btn-inverse-info btn-rounded btn-sm'
            }]
        });
    });
    $(document).ready(function() {
        $('.dataTable').DataTable();
    });

    function radioChange(e) {
        if (e.value === 'limit') {
            $('#reject_cuz').val('Dear Biz-Bazar User,Your Withdraw Request has been rejected because of your bKash limit has been exceeded.')
        } else if (e.value === 'in_job' ){
			$('#reject_cuz').val('Dear Biz-Bazar User,Your Withdraw Request has been rejected because of you have not enough job balance for withdraw.')
		}else if (e.value === 'in_refer'){
			$('#reject_cuz').val('Dear Biz-Bazar User,Your Withdraw Request has been rejected because of you have not enough refer earn balance for withdraw. .')
		}
    }

let tableIndex = null;

    function set_modal_data(e) {
		tableIndex = e.parentNode.parentNode;
        $("#_name").html($(e).data('name'));
        $("#_wrk_ern").html($(e).data('wrk_ern_amnt'));
        $("#_ref_ern").html($(e).data('ref_wrk_ern_amnt'));
        $("#_ref_bon").html($(e).data('ref_ern_amnt'));
        $("#_othr_ern").html($(e).data('other_amnt'));
        $("#_tot").html($(e).data('user_total'));
        $("#_req_amnt").html($(e).data('req-amnt'));
        $("#_rcv_num").html('0' + $(e).data("rcv-num"));
        $("#rcv_num").val('0' + $(e).data("rcv-num"));
        $("#_pm").html($(e).data('pm'));
        $("#_req_date").html($(e).data('date'));

        $("#user_id").val($(e).data('user_id'));
        $("#user_num").val($(e).data('usr_num'));
        $("#id").val($(e).data('id'));
        $('#j_amnt').attr("max", $(e).data('wrk_ern_amnt'));
        $('#ref_com_amnt').attr("max", $(e).data('ref_wrk_ern_amnt'));
        $('#ref_bon_amnt').attr("max", $(e).data('ref_ern_amnt'));
        $('#otr_bon_amnt').attr("max", $(e).data('other_amnt'));

        $('#reject_link').attr("name", $(e).data('id'));

    }

    function reject_req() {
        $('#reject').css("display", "block");
    }

    function reject_reqst(e) {
		$('.loading').show()
        let id = e.name
		let cuz = $('#reject_cuz').val()
		let user_num = $('#user_num').val()
		let data = {
			id:id,
			cuz:cuz,
			user_num:user_num
		}
        $.ajax({
            type: "post",
            url: "<?= base_url('WithdrawReqCtrl/reject_req') ?>",
            data: data,
            dataType: "json",
            success: function(response) {
				if(response.success == 1){
					$('.modal').modal('hide');
					tableIndex.remove();
					$('.loading').hide()
				}
            }
        });
    }

    function accept_req() {
        $('#accept').css("display", "block");
    }

    $('#j_amnt,#ref_com_amnt,#ref_bon_amnt,#otr_bon_amnt').on('keyup', () => {
        let j_amnt = $('#j_amnt').val();
        let ref_com_amnt = $('#ref_com_amnt').val();
        let ref_bon_amnt = $('#ref_bon_amnt').val();
        let otr_bon_amnt = $('#otr_bon_amnt').val();

        let n_j_amnt = Number(j_amnt);
        let n_ref_com_amnt = Number(ref_com_amnt);
        let n_ref_bon_amnt = Number(ref_bon_amnt);
        let n_otr_bon_amnt = Number(otr_bon_amnt);
        $('#tot_amnt').val(n_j_amnt + n_ref_com_amnt + n_ref_bon_amnt + n_otr_bon_amnt);
    });

    function set_data_modal(e) {
        $('#name').html($(e).data('name'));
        $('#status').html($(e).data('status'));
        $('#req-amount').html($(e).data('req-amount'));
        $('#paid-amount').html($(e).data('paid-amount'));
        $('#admin-snd-num').html($(e).data('admin-snd-num'));
        $('#tran-id').html($(e).data('tran-id'));
        $('#checked-at').html($(e).data('checked-at'));
        $('#requested-at').html($(e).data('requested-at'));
        $('#rcv-num').html($(e).data('rcv-num'));
        $('#pay-method').html($(e).data('pay-method'));
        $('#job-earn').html($(e).data('job-earn'));
        $('#refer-work-amnt').html($(e).data('refer-work-amnt'));
        $('#refer-bon-amnt').html($(e).data('refer-bon-amnt'));
        $('#other_amnt').html($(e).data('other_amnt'));
    }
</script> 