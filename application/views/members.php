<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-primary text-white mr-2">
			<i class="mdi mdi-account-convert"></i>
		</span>
		Members
	</h3>
	
</div>

<div class="row">
	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">All Members</h4>
				<div class="table-responsive">
					<table id="datatable" class="table dataTable display">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								<th>
									Mobile
								</th>
								<th>
									Total<br>Refered
								</th>
								<th>
									Status
								</th>
								<th>
									Actions
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($members as $member):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$member->id?>" target="_blank"><?= $member->name ?></a></td>
								<td>0<?= $member->mobile_num ?></td>
								<td><?=$member->total_refered?></td>
								<td>
								<span class="badge badge-pill <?php if($member->status=='Active'){echo 'badge-success';}elseif($member->status=='Pending'){echo 'badge-warning';}else{echo 'badge-danger';}?>"><?=$member->status?></span>
								</td>
								<td>
									<a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$member->id?>" target="_blank" class="btn btn-inverse-primary btn-sm btn-rounded"  data-toggle="tooltip" title="View Profile"><i class="mdi mdi-eye-outline"></i></a>
									<span onclick="sms_data_set(this)" data-toggle="modal" data-target="#smsModal" data-name="<?=$member->name?>" data-id="<?=$member->id?>" data-mob="<?=$member->mobile_num?>"><button class="btn btn-inverse-success btn-sm btn-rounded" data-toggle="tooltip" title="Send Message"><i class="mdi mdi-message-text-outline"></i></button></span>
									<span onclick="notify_data_set(this)" data-toggle="modal" data-target="#notifyModal" data-name="<?=$member->name?>" data-id="<?=$member->id?>" data-mob="<?=$member->mobile_num?>"><button class="btn btn-inverse-warning btn-sm btn-rounded " data-toggle="tooltip" title="Send Notification"><i class="mdi mdi-bell-outline"></i></button></span>
									<span onclick="bon_data_set(this)" data-toggle="modal" data-target="#bonusModal" data-name="<?=$member->name?>" data-id="<?=$member->id?>" data-mob="<?=$member->mobile_num?>"><button class="btn btn-inverse-info btn-sm btn-rounded <?=$member->status=='Blocked' ? 'd-none':''?><?=$member->status=='Pending' ? 'd-none':''?>" data-toggle="tooltip" title="Send Extra Bonus"><i class="mdi mdi-coin "></i></button></span>

									<button id="block<?=$member->id?>" onclick="block(this)" data-id="<?=$member->id?>" class="btn btn-inverse-danger btn-sm btn-rounded <?=$member->status=='Blocked' ? 'd-none':''?><?=$member->status=='Pending' ? 'd-none':''?>" data-toggle="tooltip" title="Block This Account"><i class="mdi mdi-close-circle-outline "></i><i class="mdi mdi-sync mdi-spin loading"></i> </button>
									<button id="unblock<?=$member->id?>" onclick="unblock(this)" data-id="<?=$member->id?>" class="btn btn-inverse-success btn-sm btn-rounded <?=$member->status=='Blocked'? '':'d-none'?> " data-toggle="tooltip" title="Unblock This Account"><i class="mdi mdi-check "></i><i class="mdi mdi-sync mdi-spin loading"></i> </button>
								</td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-danger text-white mr-2">
			<i class="mdi mdi-account-remove"></i>
		</span>
		Inactive Members
	</h3>
	
</div>

<a id="inactive_member" href="#inactive_member"> </a>
<div class="row">

	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">Inactive Members</h4>
				<div class="table-responsive">
					<table id="datatable" class="table dataTable display">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								<th>
									Mobile
								</th>
								<th>
									Status
								</th>
								<th>
									Actions
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($i_members as $member):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$member->id?>" target="_blank"> <?= $member->name ?></a></td>
								<td>0<?= $member->mobile_num ?></td>
								<td>
								<span class="badge badge-pill <?php if($member->status=='Active'){echo 'badge-success';}elseif($member->status=='Inactive'){echo 'badge-danger';}else{echo 'badge-warning';}?>"><?=$member->status?></span>
								</td>
								<td>
									<a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$member->id?>" target="_blank" class="btn btn-inverse-primary btn-sm btn-rounded"  data-toggle="tooltip" title="View Profile"><i class="mdi mdi-eye-outline"></i></a>
									<span onclick="sms_data_set(this)" data-toggle="modal" data-target="#smsModal" data-name="<?=$member->name?>" data-id="<?=$member->id?>" data-mob="<?=$member->mobile_num?>"><button class="btn btn-inverse-success btn-sm btn-rounded" data-toggle="tooltip" title="Send Message"><i class="mdi mdi-message-text-outline"></i></button></span>
									<span onclick="notify_data_set(this)" data-toggle="modal" data-target="#notifyModal" data-name="<?=$member->name?>" data-id="<?=$member->id?>" data-mob="<?=$member->mobile_num?>"><button class="btn btn-inverse-warning btn-sm btn-rounded" data-toggle="tooltip" title="Send Notification"><i class="mdi mdi-bell-outline"></i></button></span>
									<a href="<?=base_url()?>/MemberCtrl/remove_member?user_id=<?=$member->id?>" onclick="return confirm('Confirm Remove This Account ?')" class="btn btn-inverse-danger btn-sm btn-rounded" data-toggle="tooltip" title="Remove Account"><i class="mdi mdi-account-remove "></i></a>

									<button id="block<?=$member->id?>" onclick="block(this)" data-id="<?=$member->id?>" class="btn btn-inverse-danger btn-sm btn-rounded <?=$member->status=='Blocked' ? 'd-none':''?><?=$member->status=='Pending' ? 'd-none':''?>" data-toggle="tooltip" title="Block This Account"><i class="mdi mdi-close-circle-outline "></i><i class="mdi mdi-sync mdi-spin loading"></i> </button>
									<button id="unblock<?=$member->id?>" onclick="unblock(this)" data-id="<?=$member->id?>" class="btn btn-inverse-success btn-sm btn-rounded <?=$member->status=='Blocked'? '':'d-none'?> " data-toggle="tooltip" title="Unblock This Account"><i class="mdi mdi-check "></i><i class="mdi mdi-sync mdi-spin loading"></i> </button>
								</td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-warning text-white mr-2">
			<i class="mdi mdi-account-alert"></i>
		</span>
		Pending Members
	</h3>
	
</div>

<a id="pending_member" href="#pending_member"> </a>
<div class="row">

	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">Pending Members</h4>
				<div class="table-responsive">
					<table id="datatable" class="table dataTable display">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								<th>
									Mobile
								</th>
								<th>
									Status
								</th>
								<th>
									Actions
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($p_members as $member):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$member->id?>" target="_blank"><img src="<?=main_url().'/app-assets/images/members/'.$member->picture?>" alt="">  <?= $member->name ?></a></td>
								<td>0<?= $member->mobile_num ?></td>
								<td>
								<span class="badge badge-pill <?php if($member->status=='Active'){echo 'badge-success';}elseif($member->status=='Inactive'){echo 'badge-danger';}else{echo 'badge-warning';}?>"><?=$member->status?></span>
								</td>
								<td>
									<a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$member->id?>" target="_blank" class="btn btn-inverse-primary btn-sm btn-rounded"  data-toggle="tooltip" title="View Profile"><i class="mdi mdi-eye-outline"></i></a>
									<span onclick="sms_data_set(this)" data-toggle="modal" data-target="#smsModal" data-name="<?=$member->name?>" data-id="<?=$member->id?>" data-mob="<?=$member->mobile_num?>"><button class="btn btn-inverse-success btn-sm btn-rounded" data-toggle="tooltip" title="Send Message"><i class="mdi mdi-message-text-outline"></i></button></span>
									<span onclick="notify_data_set(this)" data-toggle="modal" data-target="#notifyModal" data-name="<?=$member->name?>" data-id="<?=$member->id?>" data-mob="<?=$member->mobile_num?>"><button class="btn btn-inverse-warning btn-sm btn-rounded" data-toggle="tooltip" title="Send Notification"><i class="mdi mdi-bell-outline"></i></button></span>
									<a href="<?=base_url()?>/MemberCtrl/remove_member?user_id=<?=$member->id?>" onclick="return confirm('Confirm Remove This Account ?')" class="btn btn-inverse-danger btn-sm btn-rounded" data-toggle="tooltip" title="Remove Account"><i class="mdi mdi-account-remove "></i></a>
								</td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="page-header">
	<h3 class="page-title">
		<span class="page-title-icon bg-gradient-danger text-white mr-2">
			<i class="mdi mdi-close-circle-outline"></i>
		</span>
		Blocked Members
	</h3>
	
</div>

<a id="blocked" href="#blocked"> </a>
<div class="row">

	<div class="col-12 grid-margin">
		<div class="card shadow border10">
			<div class="card-body">
				<h4 class="card-title">Blocked Members</h4>
				<div class="table-responsive">
					<table id="datatable" class="table dataTable display">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								<th>
									Mobile
								</th>
								<th>
									Status
								</th>
								<th>
									Actions
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $serial=1; foreach($b_members as $member):?>
							<tr>
								<td><?= $serial++ ?></td>
								<td><a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$member->id?>" target="_blank"><img src="<?=main_url().'/app-assets/images/members/'.$member->picture?>" alt="">  <?= $member->name ?></a></td>
								<td>0<?= $member->mobile_num ?></td>
								<td>
								<span class="badge badge-pill <?php if($member->status=='Active'){echo 'badge-success';}elseif($member->status=='Pending'){echo 'badge-warning';}else{echo 'badge-danger';}?>"><?=$member->status?></span>
								</td>
								<td>
									<a href="<?=base_url()?>/ProfileCtrl?user_id=<?=$member->id?>" target="_blank" class="btn btn-inverse-primary btn-sm btn-rounded"  data-toggle="tooltip" title="View Profile"><i class="mdi mdi-eye-outline"></i></a>
									<span onclick="sms_data_set(this)" data-toggle="modal" data-target="#smsModal" data-name="<?=$member->name?>" data-id="<?=$member->id?>" data-mob="<?=$member->mobile_num?>"><button class="btn btn-inverse-success btn-sm btn-rounded" data-toggle="tooltip" title="Send Message"><i class="mdi mdi-message-text-outline"></i></button></span>
									<span onclick="notify_data_set(this)" data-toggle="modal" data-target="#notifyModal" data-name="<?=$member->name?>" data-id="<?=$member->id?>" data-mob="<?=$member->mobile_num?>"><button class="btn btn-inverse-warning btn-sm btn-rounded" data-toggle="tooltip" title="Send Notification"><i class="mdi mdi-bell-outline"></i></button></span>

									<button id="block<?=$member->id?>" onclick="block(this)" data-id="<?=$member->id?>" class="btn btn-inverse-danger btn-sm btn-rounded <?=$member->status=='Blocked' ? 'd-none':''?><?=$member->status=='Pending' ? 'd-none':''?>" data-toggle="tooltip" title="Block This Account"><i class="mdi mdi-close-circle-outline "></i><i class="mdi mdi-sync mdi-spin loading"></i> </button>
									<button id="unblock<?=$member->id?>" onclick="unblock(this)" data-id="<?=$member->id?>" class="btn btn-inverse-success btn-sm btn-rounded <?=$member->status=='Blocked'? '':'d-none'?> " data-toggle="tooltip" title="Unblock This Account"><i class="mdi mdi-check "></i><i class="mdi mdi-sync mdi-spin loading"></i> </button>
								</td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- //send sms modal -->
<div class="modal fade" id="smsModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"> <i class="mdi mdi-phone "></i> Send SMS to <b id="s_name"></b></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

	 	<input id="s_id" type="hidden">
		<input id="s_mob" type="hidden">
		<textarea id="s_txt" cols="30" rows="10" class="form-control" placeholder="Write Here..." ></textarea>
		<br>
		<div id="s_result" class="text-center"></div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button onclick="send_sms()" type="button" class="btn btn-success">Send <i class="mdi mdi-sync mdi-spin loading"></i> </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<!-- //notify modal -->
<div class="modal fade" id="notifyModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"> <i class="mdi mdi-bell-outline"></i> Send Notification to <b id="n_name"></b></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

	 	<input id="n_id" type="hidden">
		<textarea id="n_txt" cols="30" rows="10" class="form-control" placeholder="Write Here..." ></textarea>
		<br>
		<div id="n_result" class="text-center"></div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button onclick="send_notify()" type="button" class="btn btn-warning">Send <i class="mdi mdi-sync mdi-spin loading"></i> </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<!-- //bonus modal -->
<div class="modal fade" id="bonusModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"> <i class="mdi mdi-coin"></i> Send Bonus to <b id="b_name"></b></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

	 	<input id="b_id" type="hidden">
	 	<input id="b_mob" type="hidden">
		<h6><i class="mdi mdi-cash"></i> Amount</h6>
		<input id="b_amnt" type="number" id="" class="form-control" placeholder="Give Bonus Amount...">
		<h6><i class="mdi mdi-message-outline"></i> Sms Text</h6>
		<textarea id="bs_txt" cols="30" rows="3" class="form-control" placeholder="Write Here..." >Congrats! You have got bonus from Biz Bazar</textarea>
		<h6><i class="mdi mdi-bell-outline"></i> Notification</h6>
		<textarea id="bn_txt" cols="30" rows="3" class="form-control" placeholder="Write Here..." >Congrats! You have got bonus from Biz Bazar</textarea>
		<br>
		<div id="b_result" class="text-center"></div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button onclick="send_bonus()" type="button" class="btn btn-info">Send <i class="mdi mdi-sync mdi-spin loading"></i> </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>


<script>

	$(document).ready(function () {
		$('#datatable').DataTable();
	});
	$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip(); 
});

function block(e) {
	if(confirm('Are Sure To Block This Person ?')){
		let id = $(e).data('id');
		$.ajax({
			type: "post",
			url: "<?=base_url('MemberCtrl/block_user')?>",
			data: {id:id},
			dataType: "json",
			success: function (response) {
				if(response.success == 1){
					e.innerHTML += '<i class="mdi mdi-check"></i>';
					$(e).addClass('d-none');
					$('#unblock'+$(e).data('id')).removeClass('d-none');
				}
			}
		});
	}
}

function unblock(e) {
	if(confirm('Are Sure To Unblock This Person ?')){
		let id = $(e).data('id');
		$.ajax({
			type: "post",
			url: "<?=base_url('MemberCtrl/unblock_user')?>",
			data: {id:id},
			dataType: "json",
			success: function (response) {
				if(response.success == 1){
					e.innerHTML += '<i class="mdi mdi-check"></i>';
					$(e).addClass('d-none');
					$('#block'+$(e).data('id')).removeClass('d-none');
				}
			}
		});
	}
}

function sms_data_set(e) {
	$('#s_name').html($(e).data('name'));
	$('#s_mob').val($(e).data('mob'));
}
function notify_data_set(e) {
	$('#n_name').html($(e).data('name'));
	$('#n_id').val($(e).data('id'));
}
function bon_data_set(e) {
	$('#b_name').html($(e).data('name'));
	$('#b_id').val($(e).data('id'));
	$('#b_mob').val($(e).data('mob'));
}

function send_sms() {
	$('.loading').show();
	let mob = $('#s_mob').val();
	let txt = $('#s_txt').val();

	let data ={
		mob:mob,
		txt:txt
	};
	$.ajax({
		type: "post",
		url: "<?=base_url()?>MemberCtrl/send_sms",
		data: data,
		dataType: "json",
		success: function (response) {
			if(response.success==1){
				$('.loading').hide();
				$('#s_result').html('<span class="alert alert-success">Successfully Sended.</span>');
				setTimeout(() => {
					$('#s_result').html('');
					$('#s_txt').val('');
					$('.modal').modal('hide');
				}, 2000);
			}
			else{
				$('#s_result').html('<span class="alert alert-danger">Error Occured !</span>');
				setTimeout(() => {
					$('#s_result').html('');
					$('.loading').hide();
				}, 2000);
			}
		}
	});
}

function send_notify() {
	$('.loading').show();
	let id = $('#n_id').val();
	let txt = $('#n_txt').val();

	let data ={
		user_id:id,
		txt:txt
	};
	$.ajax({
		type: "post",
		url: "<?=base_url()?>MemberCtrl/send_notify",
		data: data,
		dataType: "json",
		success: function (response) {
			if(response.success==1){
				$('.loading').hide();
				$('#n_result').html('<span class="alert alert-success">Successfully Sended.</span>');
				setTimeout(() => {
					$('#n_result').html('');
					$('#n_txt').val('');
					$('.modal').modal('hide');
				}, 2000);
			}
			else{
				$('#n_result').html('<span class="alert alert-danger">Error Occured !</span>');
				setTimeout(() => {
					$('#n_result').html('');
					$('.loading').hide();
				}, 2000);
			}
		}
	});
}

function send_bonus() {
	$('.loading').show();
	let id = $('#b_id').val();
	let mob = $('#b_mob').val();
	let amnt = $('#b_amnt').val();
	let n_txt = $('#bn_txt').val();
	let s_txt = $('#bs_txt').val();

	let data ={
		user_id:id,
		mob:mob,
		amnt:amnt,
		n_txt:n_txt,
		s_txt:s_txt
	};
	$.ajax({
		type: "post",
		url: "<?=base_url()?>MemberCtrl/send_bonus",
		data: data,
		dataType: "json",
		success: function (response) {
			if(response.success==1){
				$('.loading').hide();
				$('#b_result').html('<span class="alert alert-success">Successfully Sended.</span>');
				setTimeout(() => {
					$('#b_result').html('');
					$('#b_amnt').val('');
					$('.modal').modal('hide');
				}, 2000);
			}
			else{
				$('#b_result').html('<span class="alert alert-danger">Error Occured !</span>');
				setTimeout(() => {
					$('#b_result').html('');
					$('.loading').hide();
				}, 2000);
			}
		}
	});
}
</script>
