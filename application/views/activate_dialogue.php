<div class="row">
	<div class="col-12 alert alert-danger justify-content-center text-center">
		<br>
		<p>Please Activate Your account.</p>
			<a href="<?= base_url() ?>/ActivateCtrl" class="btn btn-danger btn-rounded shadow">Activate</a>
		<br>
		<br>
	</div>
</div>