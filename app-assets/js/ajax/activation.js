/* author:md-arif-un;

email:arifunctg@gmail.com

linkedin: /in/arif-un */





function active_req() {

	let u_id = user_id; // user id define in footer form sesison

	let pin = $('#ac_pin').val();

	let sender_number = $('#sndr_num').val();

	let transection_id = $('#tran_id').val();

	let data = {

		usr_id: u_id,

		pin: pin,

		sender_num: sender_number,

		tran_id: transection_id

    };



	if (u_id != '' && pin != '' && sender_number != "null" && transection_id != '') {

        $.ajax({

            type: "post",

            url: main_url+"ActivateCtrl/active_reqst",

            data: data,

            // dataType: "json",

            success: function (response) {

               if(response.success == 1) {

                   window.location.href = main_url+"DashboardCtrl";

               }

               else{

                $("#activ_rst").html(`<span class="alert alert-danger btn-rounded">${response.success}</span>`);

                setTimeout(() => {

                    $("#activ_rst").html(``);

                }, 4000);

               }

            }

        });

	}

}

